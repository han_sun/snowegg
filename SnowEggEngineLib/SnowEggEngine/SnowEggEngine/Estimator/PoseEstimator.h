//
//  PoseEstimator.h
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#ifndef __POSEESTIMATOR_H__
#define __POSEESTIMATOR_H__

#include <vector>
#include "../Matcher/MatchedPair.h"
#include "../Camera/Camera.h"

namespace SnowEgg
{
    namespace Estimator
    {
        class CPoseEstimator
        {
        public:
            CPoseEstimator();
            virtual ~CPoseEstimator();
            
        public:
            //virtual bool Calculate(std::vector<Matcher::CMatchedPair>* pListMatchedPair, Camera::CCamera* pCam) = 0;
        };
    }
}

#endif /* defined(__POSEESTIMATOR_H__) */
