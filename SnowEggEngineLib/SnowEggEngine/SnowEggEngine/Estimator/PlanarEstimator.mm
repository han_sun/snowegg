//
//  ProSACEstimator.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#include "PlanarEstimator.h"

using namespace SnowEgg;
using namespace Estimator;

typedef struct _CompareDistance
{
    bool operator()(const Matcher::C2DMatchedPair* p, const Matcher::C2DMatchedPair* q) const
    {
        return p->GetDistance() < q->GetDistance();
    }
} COMPARE_DISTANCE;

CPlanarEstimator::CPlanarEstimator()
{
    m_dReproErrOutlier = REPROERR_OUTLIER;
    m_dReproErrPROSAC = REPROERR_PROSAC;
    m_dConfidence = NUM_CONFIDENCE;
    m_iNumMaxIter = NUM_MAX_EST_ITER;
    
    m_homography.m[0][0] = 1.0; m_homography.m[0][1] = 0.0; m_homography.m[0][2] = 0.0;
    m_homography.m[1][0] = 0.0; m_homography.m[1][1] = 1.0; m_homography.m[1][2] = 0.0;
    m_homography.m[2][0] = 0.0; m_homography.m[2][1] = 0.0; m_homography.m[2][2] = 1.0;
}

CPlanarEstimator::~CPlanarEstimator()
{

}

bool CPlanarEstimator::Calculate(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair, Camera::CMonocularCam* pCam)
{
    if (NULL == pListMatchedPair || NULL == pCam)
    {
        return false;
    }
    
    if (!this->ProSAC(pListMatchedPair, pCam))
    {
        return false;
    }
    
    this->RemoveOutlier(pListMatchedPair);
    
    if (!this->Refine(pListMatchedPair))
    {
        return false;
    }

    this->DecomposeHomography(pCam);
    
    return true;
}

bool CPlanarEstimator::ProSAC(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair, Camera::CMonocularCam* pCam)
{
    int iListSize = static_cast<int>(pListMatchedPair->size());
    
    if (iListSize < NUM_MIN_PROSAC)
    {
        return false;
    }
    
    sort(pListMatchedPair->begin(), pListMatchedPair->end(), COMPARE_DISTANCE());
    
    std::vector<CvPoint2D64f> samplingObject;
    samplingObject.resize(4);
    std::vector<CvPoint2D64f> samplingReference;
    samplingReference.resize(4);
    CvMat samplingObjectPoints = cvMat(1, 4, CV_64FC2, &(samplingObject[0]));
    CvMat samplingReferencePoints = cvMat(1, 4, CV_64FC2, &(samplingReference[0]));
    
    double h[9] = {0.0f};
    double bestHomography[9] = {0.0f};
    int iBestCount = 0;
    CvMat _h = cvMat(3, 3, CV_64F, h);
    CvRNG rng = cvRNG(cvGetTickCount());
    
    int iNumSampling = NUM_SAMPLING;
    int iMaxIter = m_iNumMaxIter;
    
    for(int i = 0; i < iMaxIter; ++i)
    {
        // reset
        for(int j=0; j < iListSize; ++j)
        {
            (*pListMatchedPair)[j]->SetIsInliner(false);
        }
        
        // sampling
        double Tn1 = (double)iNumSampling;
        double Tn = Tn1 * (double)(iListSize + 1) / (double)(iListSize + 1 - iNumSampling);
        iNumSampling = iNumSampling + (int)(Tn - Tn1 + 1.0f);
        iNumSampling = MIN(iListSize-1, iNumSampling);
        
        int index[4] = {-1, -1, -1, -1};
        for(int j = 0; j < 4; ++j)
        {
            int tempIndex = cvRandInt(&rng) % iNumSampling;
            while(index[0] == tempIndex || index[1] == tempIndex || index[2] == tempIndex)
            {
                tempIndex = cvRandInt(&rng) % iNumSampling;
            }
            index[j] = tempIndex;
        }
        
        for(int j = 0; j < 4; ++j)
        {
            int tempIndex = index[j];
            
            samplingObject[j].x = (*pListMatchedPair)[tempIndex]->GetSceFeaturePoint()->GetPosition().x;
            samplingObject[j].y = (*pListMatchedPair)[tempIndex]->GetSceFeaturePoint()->GetPosition().y;
            
            samplingReference[j].x = (*pListMatchedPair)[tempIndex]->GetRefFeaturePoint()->GetPosition().x;
            samplingReference[j].y = (*pListMatchedPair)[tempIndex]->GetRefFeaturePoint()->GetPosition().y;
        }
        
        // calculate homograpy, from object plane to image plane, RANSAC is the default algorithm
        cvFindHomography(&samplingReferencePoints, &samplingObjectPoints, &_h);
        
        int inlinerCount = 0;
        // calculate consensus set
        for(int j = 0; j < iListSize; ++j)
        {
            double error = CalReproErr(*((*pListMatchedPair)[j]->GetRefFeaturePoint()),
                                       *((*pListMatchedPair)[j]->GetSceFeaturePoint()),
                                       h);
            if(error < m_dReproErrPROSAC)
            {
                (*pListMatchedPair)[j]->SetIsInliner(true);
                ++inlinerCount;
            }
        }
        
        if(inlinerCount > iBestCount)
        {
            iBestCount = inlinerCount;
            for(int k = 0; k < 9; ++k)
            {
                bestHomography[k] = h[k];
            }
            
            iMaxIter = UpdateIterNum(m_dConfidence, (double)(iListSize - inlinerCount)/(double)iListSize, 4, m_iNumMaxIter);
        }
    }
    
    // terminate
    if(iBestCount >= 4)
    {
        for(int j = 0; j < iListSize; ++j)
        {
            double error = CalReproErr(*((*pListMatchedPair)[j]->GetRefFeaturePoint()),
                                       *((*pListMatchedPair)[j]->GetSceFeaturePoint()),
                                       bestHomography);
            if(error < m_dReproErrPROSAC)
            {
                (*pListMatchedPair)[j]->SetIsInliner(true);
            }
            else
            {
                (*pListMatchedPair)[j]->SetIsInliner(false);
            }
        }
        
        std::vector<CvPoint2D64f> consensusReference;
        std::vector<CvPoint2D64f> consensusObject;
        
        for(int j = 0; j < iListSize; ++j)
        {
            if((*pListMatchedPair)[j]->IsInliner())
            {
                consensusReference.push_back(cvPoint2D64f((*pListMatchedPair)[j]->GetRefFeaturePoint()->GetPosition().x,
                                                          (*pListMatchedPair)[j]->GetRefFeaturePoint()->GetPosition().y));
                consensusObject.push_back(cvPoint2D64f((*pListMatchedPair)[j]->GetSceFeaturePoint()->GetPosition().x,
                                                       (*pListMatchedPair)[j]->GetSceFeaturePoint()->GetPosition().y));
            }
        }
        
        CvMat consensusReferencePoints = cvMat(1, static_cast<int>(consensusReference.size()), CV_64FC2, &(consensusReference[0]));
        CvMat consensusObjectPoints = cvMat(1, static_cast<int>(consensusObject.size()), CV_64FC2, &(consensusObject[0]));
        
        cvFindHomography(&consensusReferencePoints, &consensusObjectPoints, &_h);
        
        // update
        for(int i=0; i<9; i++)
        {
            m_homography.m1[i] = h[i];
        }
        
        //this->DecomposeHomography(pCam);
        
        return true;
    }
    
    return false;
}

void CPlanarEstimator::SetMaxIterationNum(int iMaxIter)
{
    m_iNumMaxIter = iMaxIter;
}

void CPlanarEstimator::SetConfidence(double dConfidence)
{
    m_dConfidence = dConfidence;
}

bool CPlanarEstimator::DecomposeHomography(Camera::CMonocularCam* pCam)
{
    if(NULL == pCam)
    {
        return false;
    }
    
    float localHomography[9] = {0.0f};
    float intrinsicMatrix[9] = {0.0f};
    float extrinsicMatrix[12] = {0.0f};
    
    for(int i = 0; i < 9; ++i)
    {
        localHomography[i] = (float)this->m_homography.m1[i];
    }
    
    for(int y = 0; y < 3 ; ++y)
    {
        for(int x = 0; x < 3; ++x)
        {
            intrinsicMatrix[y*3 + x] = (float)cvGetReal2D(pCam->GetIntrinsicMatrix(), y, x);
        }
    }
    
    CvMat _homography = cvMat(3, 3, CV_32F, localHomography);
    CvMat _intrinsic = cvMat(3, 3, CV_32F, intrinsicMatrix);
    CvMat _extrinsic = cvMat(3, 4, CV_32FC1, extrinsicMatrix);
    
    this->DecomposeHomography2RT(&_intrinsic, &_homography, &_extrinsic);
    
    for(int y = 0; y < 3 ; ++y)
    {
        for(int x = 0; x < 4; ++x)
        {
            cvSetReal2D(pCam->GetExtrinsicMatrix(), y, x, extrinsicMatrix[y*4+x]);
        }
    }
    
    return true;
}

void CPlanarEstimator::DecomposeHomography2RT(CvMat* pMatIntr, CvMat* pMatHomograhpy, CvMat* pMatRT)
{
    if(pMatHomograhpy != NULL)
    {
        CvMat *invIntrinsic = cvCloneMat(pMatIntr);
        cvInv(pMatIntr, invIntrinsic);
        
        // Vectors holding columns of H and R:
        float a_H1[3];
        CvMat  m_H1 = cvMat( 3, 1, CV_32FC1, a_H1 );
        for(int i = 0; i < 3; ++i)
        {
            cvmSet(&m_H1, i, 0, cvmGet( pMatHomograhpy, i, 0 ));
        }
        
        float a_H2[3];
        CvMat  m_H2 = cvMat( 3, 1, CV_32FC1, a_H2 );
        for(int i = 0; i < 3; ++i)
        {
            cvmSet(&m_H2, i, 0, cvmGet( pMatHomograhpy, i, 1 ));
        }
        
        float a_H3[3];
        CvMat  m_H3 = cvMat( 3, 1, CV_32FC1, a_H3 );
        for(int i = 0; i < 3; ++i)
        {
            cvmSet(&m_H3, i, 0, cvmGet( pMatHomograhpy, i, 2 ));
        }
        
        float a_CinvH1[3];
        CvMat  m_CinvH1 = cvMat( 3, 1, CV_32FC1, a_CinvH1 );
        
        float a_R1[3];
        CvMat  m_R1 = cvMat( 3, 1, CV_32FC1, a_R1 );
        
        float a_R2[3];
        CvMat  m_R2 = cvMat( 3, 1, CV_32FC1, a_R2 );
        
        float a_R3[3];
        CvMat  m_R3 = cvMat( 3, 1, CV_32FC1, a_R3 );
        
        // The rotation matrix:
        float a_R[9];
        CvMat  m_R = cvMat( 3, 3, CV_32FC1, a_R );
        
        // The translation vector:
        float a_T[3];
        CvMat  m_T = cvMat( 3, 1, CV_32FC1, a_T );
        
        ////////////////////////////////////////////////////////
        // Create norming factor lambda:
        cvGEMM(invIntrinsic, &m_H1, 1, NULL, 0, &m_CinvH1, 0 );
        
        // Search next orthonormal matrix:
        if( cvNorm( &m_CinvH1, NULL, CV_L2, NULL ) != 0 )
        {
            float lambda = (float)(1.0f/cvNorm( &m_CinvH1, NULL, CV_L2, NULL ));
            
            // Create normalized R1 & R2:
            cvGEMM( invIntrinsic, &m_H1, lambda, NULL, 0, &m_R1, 0 );
            cvGEMM( invIntrinsic, &m_H2, lambda, NULL, 0, &m_R2, 0 );
            
            // Get R3 orthonormal to R1 and R2:
            cvCrossProduct( &m_R1, &m_R2, &m_R3 );
            
            // Put the rotation column vectors in the rotation matrix:
            for(int i = 0; i < 3; ++i)
            {
                cvmSet(&m_R, i, 0,  cvmGet( &m_R1, i, 0 ));
                cvmSet(&m_R, i, 1,  cvmGet( &m_R2, i, 0 ));
                cvmSet(&m_R, i, 2,  cvmGet( &m_R3, i, 0 ));
            }
            
            // Calculate Translation Vector T (- because of its definition):
            cvGEMM(invIntrinsic, &m_H3, lambda, NULL, 0, &m_T, 0);
            
            // Transformation of R into - in Frobenius sense - next orthonormal matrix:
            float a_W[9]; CvMat  m_W  = cvMat(3, 3, CV_32FC1, a_W );
            float a_U[9]; CvMat  m_U  = cvMat(3, 3, CV_32FC1, a_U );
            float a_Vt[9]; CvMat  m_Vt = cvMat(3, 3, CV_32FC1, a_Vt);
            cvSVD(&m_R, &m_W, &m_U, &m_Vt, CV_SVD_MODIFY_A | CV_SVD_V_T);
            cvMatMul(&m_U, &m_Vt, &m_R);
            
            cvReleaseMat(&invIntrinsic);
            
            cvSetIdentity(pMatRT);
            for(int i = 0; i < 3; ++i)
            {
                for(int j = 0; j < 3; ++j)
                {
                    cvmSet(pMatRT, i, j, cvmGet(&m_R, i, j));
                }
                cvmSet(pMatRT, i, 3, cvmGet(&m_T, i, 0));
            }
        }
    }
}

bool CPlanarEstimator::RemoveOutlier(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair)
{
    // TODO: replace vector to link, speed up the removal
    int iSize = static_cast<int>(pListMatchedPair->size());
    double dErr = 0.0f;
    CVector3f vecImgPoint;
    
    for(int i = 0; i < iSize; ++i)
    {
        vecImgPoint = this->CvtObj2Img((*pListMatchedPair)[i]->GetRefFeaturePoint()->GetPosition());
        vecImgPoint /= vecImgPoint.z;
        
        dErr = (*pListMatchedPair)[i]->GetSceFeaturePoint()->GetPosition().GetDistance(vecImgPoint);
        if(dErr >= m_dReproErrOutlier)
        {
            SAFE_DELETE((*pListMatchedPair)[i]);
            pListMatchedPair->erase(pListMatchedPair->begin() + i);
            --i;
            --iSize;
        }
    }
    
    return true;
}

bool CPlanarEstimator::Refine(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair)
{
    int iSize = static_cast<int>(pListMatchedPair->size());
    
    if (iSize < NUM_SAMPLING)
    {
        return false;
    }
    
    CvLevMarq solver(8, 0, cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, NUM_MAX_REFITER, DBL_EPSILON));
    CvMat modelPart = cvMat(solver.param->rows, solver.param->cols, CV_64F, m_homography.m1 );
    cvCopy(&modelPart, solver.param);
    
    while (true)
    {
        const CvMat* _param = NULL;
        CvMat* _JtJ = NULL;
        CvMat* _JtErr = NULL;
        double* _errNorm = NULL;
        
        if (!solver.updateAlt(_param, _JtJ, _JtErr, _errNorm))
        {
            break;
        }
        
        for (int i = 0; i < iSize; ++i)
        {
            const double* h = _param->data.db;
            double Mx = (*pListMatchedPair)[i]->GetRefFeaturePoint()->GetPosition().x;
            double My = (*pListMatchedPair)[i]->GetRefFeaturePoint()->GetPosition().y;
            double mx = (*pListMatchedPair)[i]->GetSceFeaturePoint()->GetPosition().x;
            double my = (*pListMatchedPair)[i]->GetSceFeaturePoint()->GetPosition().y;
            
            // calculate the projection of souce point on target plane with H
            // translation
            double ww = 1.0f/(h[6]*Mx + h[7]*My + 1.0f);
            // rotation
            double _xi = (h[0]*Mx + h[1]*My + h[2]) * ww;
            double _yi = (h[3]*Mx + h[4]*My + h[5]) * ww;
            
            double err[] = {_xi - mx, _yi - my};
            if (NULL != _JtJ || NULL != _JtErr)
            {
                // _xi, _yi
                double J[][8] =
                {
                    {Mx*ww, My*ww, ww, 0, 0, 0, -Mx*ww*_xi, -My*ww*_xi},
                    {0, 0, 0, Mx*ww, My*ww, ww, -Mx*ww*_yi, -My*ww*_yi}
                };
                
                for (int j = 0; j < 8; ++j)
                {
                    for (int k = j; k < 8; ++k)
                    {
                        _JtJ->data.db[j*8+k] += J[0][j]*J[0][k] + J[1][j]*J[1][k];
                    }
                    _JtErr->data.db[j] += J[0][j]*err[0] + J[1][j]*err[1];
                }
            }
            
            // Symmetic transfer error
            if (NULL != _errNorm)
            {
                *_errNorm += err[0]*err[0] + err[1]*err[1];
            }
        }
    }
    
    cvCopy(solver.param, &modelPart);
    
    return true;
}

double CPlanarEstimator::CalReproErr(Feature::CFeaturePoint refFP, Feature::CFeaturePoint sceFP, double* pHomography)
{
    double ww = 1.0f/(pHomography[6] * refFP.GetPosition().x + pHomography[7] * refFP.GetPosition().y + pHomography[8]);
    double x = (pHomography[0] * refFP.GetPosition().x + pHomography[1] * refFP.GetPosition().y +  pHomography[2]) * ww;
    double y = (pHomography[3] * refFP.GetPosition().x + pHomography[4] * refFP.GetPosition().y +  pHomography[5]) * ww;
    double error = sqrt((sceFP.GetPosition().x - x)*(sceFP.GetPosition().x - x)) + sqrt((sceFP.GetPosition().y - y)*(sceFP.GetPosition().y - y));
    return error;
}

int CPlanarEstimator::UpdateIterNum(double p, double ep, int iPointNum, int iMaxIter)
{
    double num = 0.0f;
    double denom = 0.0f;
    p = MAX(p, 0.0f);
    p = MIN(p, 1.0f);
    ep = MAX(ep, 0.0f);
    ep = MIN(ep, 1.0f);
    
    // avoid inf's & nan's
    num = MAX(1.0f - p, DBL_MIN);
    denom = 1.0f - pow(1.0f - ep, iPointNum);
    num = log(num);
    denom = log(denom);
    
    int result = denom >= 0 || -num >= iMaxIter*(-denom) ? iMaxIter : cvRound(num/denom);
    return result;
}

CVector3f CPlanarEstimator::CvtObj2Img(CVector3f point)
{
    return m_homography * point;
}

CVector3f CPlanarEstimator::CvtImg2Obj(CVector3f point)
{
    return m_homography.Inverse() * point;
}