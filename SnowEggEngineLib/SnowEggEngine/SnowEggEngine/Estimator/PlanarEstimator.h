//
//  ProSACEstimator.h
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#ifndef __PLANARESTIMATOR_H__
#define __PLANARESTIMATOR_H__

#include "PoseEstimator.h"
#include "../Base/Matrix33f.h"
#include "../Matcher/2DMatchedPair.h"
#include "../Camera/MonocularCam.h"

namespace SnowEgg
{
    namespace Estimator
    {
        class CPlanarEstimator : virtual public CPoseEstimator
        {
        public:
            CPlanarEstimator();
            virtual ~CPlanarEstimator();
            
        public:
            virtual bool Calculate(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair, Camera::CMonocularCam* pCam);
            
        public:
            void SetMaxIterationNum(int iMaxIter);
            void SetConfidence(double dConfidence);
            
        protected:
            bool ProSAC(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair, Camera::CMonocularCam* pCam);
            bool DecomposeHomography(Camera::CMonocularCam* pCam);
            void DecomposeHomography2RT(CvMat* pMatIntr, CvMat* pMatHomograhpy, CvMat* pMatRT);
            bool RemoveOutlier(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair);
            bool Refine(std::vector<Matcher::C2DMatchedPair*>* pListMatchedPair);
            
        protected:
            double CalReproErr(Feature::CFeaturePoint refFP, Feature::CFeaturePoint sceFP, double* pHomography);
            int UpdateIterNum(double p, double ep, int iPointNum, int iMaxIter);
            CVector3f CvtObj2Img(CVector3f point = CVector3f(0.0f, 0.0f, 1.0f));
            CVector3f CvtImg2Obj(CVector3f point = CVector3f(0.0f, 0.0f, 1.0f));
            
        protected:
            double m_dConfidence;
            int m_iNumMaxIter;
            double m_dReproErrPROSAC;
            double m_dReproErrOutlier;
            CMatrix33f m_homography;
        };
    }
}

#endif /* defined(__PLANARESTIMATOR_H__) */
