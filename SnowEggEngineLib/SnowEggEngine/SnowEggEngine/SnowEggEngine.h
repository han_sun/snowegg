//
//  SnowEggEngine.h
//  SnowEggEngine
//
//  Created by Han Sun on 24/02/2015.
//
//
#ifndef __SNOWEGGENGINE_H__
#define __SNOWEGGENGINE_H__

namespace SnowEgg
{
    struct SEBinding_Struct_ARStateNativeMap
    {
        // TODO: char szTrackableID[MAX_LENGTH_ID] = {'\0'};
        int iTrackableID = 1;
        float fCamPosX = 0.0f;
        float fCamPosY = 0.0f;
        float fCamPosZ = 0.0f;
        float fCamRotX = 0.0f;
        float fCamRotY = 0.0f;
        float fCamRotZ = 0.0f;
    };
    
    struct SEBinding_Struct_ARStateQuatMap
    {
        // TODO: char szTrackableID[MAX_LENGTH_ID] = {'\0'};
        int iTrackableID = 1;
        float fCamPosX = 0.0f;
        float fCamPosY = 0.0f;
        float fCamPosZ = 0.0f;
        float fCamQuatA = 0.0f;
        float fCamQuatB = 0.0f;
        float fCamQuatC = 0.0f;
        float fCamQuatD = 0.0f;
    };
    
    class CSnowEggEngine
    {
    public:
        static CSnowEggEngine* GetInstance();
        
    public:
        // Start the camera automatically
        bool Activate();
        bool Deactivate();
        
    public:
        bool StartTracking();
        bool StopTracking();
        
    public:
        void RenderCameraFrame();
        int GetTextureID();
        int GetOrNewTextureID();
        int GetTextureWidth();
        int GetTextureHeight();
        int GetRenderWidth();
        int GetRenderHeight();
        
    public:
        // return a list of trackable status, which is thread safe
        // Call ReleaseBuffer function from Unity after using the return buffer
        // The return value follows this format (ignore '[', ']' and value types):
        // [int iCount]
        // [int iTrackableID0, float[3][3] fMatIntri, float[4][4] fMatExtri]
        // [int iTrackableID1, float[3][3] fMatIntri, float[4][4] fMatExtri]
        // ...
        // [int iTrackableID2, float[3][3] fMatIntri, float[4][4] fMatExtri]
        char* GetCurState() const;
        // The return value follows this format (ignore '[', ']' and value types):
        // [int iCount]
        // [int iTrackableID0, float[3] fCamPosition, float[3]fCamRotation]
        // [int iTrackableID1, float[3] fCamPosition, float[3]fCamRotation]
        // ...
        // [int iTrackableID2, float[3] fCamPosition, float[3]fCamRotation]
        char* GetCurSimpleState() const;
        // pElementsNum input the max tracking number, and get the number of actual tracking back.
        // No need to call ReleaseBuffer.
        int GetARState(SEBinding_Struct_ARStateNativeMap* pOutArr, int* pElementsNum);
        int GetARStateQuaternion(SEBinding_Struct_ARStateQuatMap* pOutArr, int* pElementsNum);
    
    public:
        bool ReleaseBuffer(char* szBuf);
        
    public:
        // Return ID of new showcase, if failed, return NULL
        // Input Type 0 to create a planar Showcase
        char* CreateShowcase(char* szShowcaseID, int iMaxTracking = 1, int iShowcaseType = 0);
        bool ReleaseShowcase(char* szShowcaseID);
        int GetShowcaseCount() const;
        bool EnableShowcase(const char* szShowcaseID);
        bool DisableShowcase(const char* szShowcaseID);
        
    public:
        // Return MarkerID. Return -1 if failed.
        bool CreateMarker(char* szMarkerSource, int iMarkerType, char* szShowcaseID, char* szMarkerID, bool bAutoLoad = true);
        bool ReleaseMarker(char* szMarkerID, char* szShowcaseID = NULL);
        bool LoadMarker(char* szMarkerID, char* szShowcaseID = NULL);
        bool UnloadMarker(char* szMarkerID, char* szShowcaseID = NULL);
        int GetMarkerCount(char* szShowcaseID) const;
        
    private:
        CSnowEggEngine();
        // Don't implement this forever
        CSnowEggEngine(CSnowEggEngine const&);
        // Don't implement this forever
        void operator=(CSnowEggEngine const&);
    };
}

#endif /* defined(__SNOWEGGENGINE_H__) */