//
//  CCameraDeviceObjCBridge.mm
//  SnowEgg
//
//  Created by Adrian Butt on 9/04/2015.
//  Copyright (c) 2015 Auggd. All rights reserved.
//

#ifndef __OBJC__
#error Objective-C is required for this bridge!
#endif

#include "CCameraDevice.h"
#import <Foundation/Foundation.h>
#import "CameraDevice.h"

using namespace SnowEgg;

@interface CameraDeviceBridge : NSObject <CameraDeviceDelegate>
{
    CameraDevice*               m_cameraDevice;
    CALL_BACK_IMGPROC           m_camFeedUpdatedCB;
    void*                       m_camFeedUpdatedCBContext;
}

@property (readonly) CameraDevice* camDevice;

@end

@implementation CameraDeviceBridge

- (CameraDevice*)camDevice
{
    return m_cameraDevice;
}

-(id) init
{
    if(self = [super init])
    {
        m_cameraDevice          = [[CameraDevice alloc] init];
        [m_cameraDevice setDelegate:self];
    }
    
    return self;
}

-(void) dealloc
{
    if(m_cameraDevice != nil)
    {
        [m_cameraDevice release];
        m_cameraDevice = nil;
    }
    m_camFeedUpdatedCB          = nil;
    m_camFeedUpdatedCBContext   = nil;
    
    [super dealloc];
}

- (void)onCameraFrameUpdated:(cv::Mat&)image camDevice:(CameraDevice*)camDevice
{
    if(m_camFeedUpdatedCB != nil)
    {
        m_camFeedUpdatedCB(image, m_camFeedUpdatedCBContext);
    }
}

- (void)setCamFeedUpdatedCB:(CALL_BACK_IMGPROC)toCB context:(void*)context
{
    m_camFeedUpdatedCB          = toCB;
    m_camFeedUpdatedCBContext   = context;
}

@end

namespace SnowEgg
{
    
    class CCameraDeviceImpl
    {
    public:
        CameraDeviceBridge*     bridge;
        
    public:
        CCameraDeviceImpl(CCameraDevice* device)
        {
            camDevice   = device;
            bridge      = [[CameraDeviceBridge alloc] init];
        }
        
        ~CCameraDeviceImpl()
        {
            if(bridge != nil)
            {
                [bridge release];
                bridge = nil;
            }
        }
    private:
        CCameraDevice*          camDevice;
    };
    
    CCameraDevice::CCameraDevice()
    {
        impl            = new CCameraDeviceImpl(this);
    }
    CCameraDevice::~CCameraDevice()
    {
        if(impl)
        {
            delete impl;
        }
    }
    
    void    CCameraDevice::SetCamFeedUpdatedCallback(CALL_BACK_IMGPROC toCB, void* context)
    {
        [impl->bridge setCamFeedUpdatedCB:toCB context:context];
    }
    bool    CCameraDevice::InitCam()
    {
        return [impl->bridge.camDevice initCam];
    }
    void    CCameraDevice::StartCam()
    {
        [impl->bridge.camDevice startCam];
    }
    void    CCameraDevice::SetTextureID(int toId, int texW, int texH, int texFormat)
    {
        [impl->bridge.camDevice setTextureID:toId texW:texW texH:texH texFormat:texFormat];
    }
    void    CCameraDevice::RenderCurrentCameraFrameToTexture()
    {
        [impl->bridge.camDevice renderCurrentCameraFrameToTexture];
    }
    int 	CCameraDevice::GetTextureID(bool createIfNone)
    {
        return [impl->bridge.camDevice getTextureID:createIfNone];
    }
    int     CCameraDevice::GetTextureWidth()
    {
        return [impl->bridge.camDevice getTextureWidth];
    }
    int     CCameraDevice::GetTextureHeight()
    {
        return [impl->bridge.camDevice getTextureHeight];
    }
    int     CCameraDevice::GetRenderWidth()
    {
        return [impl->bridge.camDevice getRenderWidth];
    }
    int     CCameraDevice::GetRenderHeight()
    {
        return [impl->bridge.camDevice getRenderHeight];
    }
}
