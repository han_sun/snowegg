//
//  SnowEggEngine.m
//  SnowEggEngine
//
//  Created by Han Sun on 24/02/2015.
//
//

#import <UIKit/UIKit.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#include <math.h>
#include "OpenCVIOSAdapt.h"
#include "SnowEggEngine.h"
#include "EngineCore.h"
#include "PlanarShowcase.h"

using namespace SnowEgg;

CEngineCore* g_pEngineCore = NULL;

// Put the texture render code here.
// This function will be called after a frame is processed
void RenderCallback(cv::Mat& image)
{
    
}

CSnowEggEngine* CSnowEggEngine::GetInstance()
{
    static CSnowEggEngine instance;
    return &instance;
}

CSnowEggEngine::CSnowEggEngine()
{
    if (NULL == g_pEngineCore)
    {
        g_pEngineCore = CEngineCore::GetInstance();
    }
}

bool CSnowEggEngine::Activate()
{
    if (NULL == g_pEngineCore)
    {
        return false;
    }
    
    g_pEngineCore->InitEngine();
    g_pEngineCore->StartEngine();
    
    return true;
}

bool CSnowEggEngine::Deactivate()
{
    if (NULL == g_pEngineCore)
    {
        return false;
    }
    
    g_pEngineCore->StopEngine();
    g_pEngineCore->StopTracking();
    
    g_pEngineCore->ReleaseAllShowcase();
    
    return true;
}

bool CSnowEggEngine::StartTracking()
{
    if (NULL == g_pEngineCore)
    {
        return false;
    }
    
    g_pEngineCore->StartTracking();
    
    return true;
}

bool CSnowEggEngine::StopTracking()
{
    if (NULL == g_pEngineCore)
    {
        return false;
    }
    
    g_pEngineCore->StopTracking();
    
    return true;
}

void CSnowEggEngine::RenderCameraFrame()
{
    if (NULL != g_pEngineCore)
    {
        g_pEngineCore->GetCameraDevice()->RenderCurrentCameraFrameToTexture();
    }
}

int CSnowEggEngine::GetTextureID()
{
    if (NULL != g_pEngineCore)
    {
        return g_pEngineCore->GetCameraDevice()->GetTextureID(NO);
    }
    
    return 0;
}

int CSnowEggEngine::GetOrNewTextureID()
{
    if (NULL != g_pEngineCore)
    {
        return g_pEngineCore->GetCameraDevice()->GetTextureID(YES);
    }
    
    return 0;
}

int CSnowEggEngine::GetTextureWidth()
{
    if (NULL != g_pEngineCore)
    {
        return g_pEngineCore->GetCameraDevice()->GetTextureWidth();
    }
    
    return -1;
}

int CSnowEggEngine::GetTextureHeight()
{
    if (NULL != g_pEngineCore)
    {
        return g_pEngineCore->GetCameraDevice()->GetTextureHeight();
    }
    
    return -1;
}

int CSnowEggEngine::GetRenderWidth()
{
    if (NULL != g_pEngineCore)
    {
        return g_pEngineCore->GetCameraDevice()->GetRenderWidth();
    }
    
    return -1;
}

int CSnowEggEngine::GetRenderHeight()
{
    if (NULL != g_pEngineCore)
    {
        return g_pEngineCore->GetCameraDevice()->GetRenderHeight();
    }
    
    return -1;
}

bool CSnowEggEngine::EnableShowcase(const char *szShowcaseID)
{
    if (NULL == szShowcaseID)
    {
        return false;
    }
    
    Showcase::CShowcase* pShowcase = g_pEngineCore->GetShowcase(szShowcaseID);
    
    if (NULL == pShowcase)
    {
        return false;
    }
    
    pShowcase->Enable();
    
    return true;
};

bool CSnowEggEngine::DisableShowcase(const char *szShowcaseID)
{
    if (NULL == szShowcaseID)
    {
        return false;
    }
    
    Showcase::CShowcase* pShowcase = g_pEngineCore->GetShowcase(szShowcaseID);
    
    if (NULL == pShowcase)
    {
        return false;
    }
    
    pShowcase->Disable();
    
    return true;
}

int CSnowEggEngine::GetARState(SEBinding_Struct_ARStateNativeMap* pOutArr, int* pElementsNum)
{
    // the output array is a pre-generated array numelements long.
    //   we will modify num elements to match the amount of entries we will return
    if (NULL == g_pEngineCore || *pElementsNum <= 0)
    {
        // we at least need some data to modify!
        return API_ERR_GENERAL;
    }
    
    int errorCode = API_ERR_NONE;

    int iShowcaseCount = g_pEngineCore->GetShowcaseCount();
    int iTrackableCount = 0;
    Showcase::CShowcase* pShowcase = NULL;
    Repository::CTrackable* pTrackable = NULL;
    // TODO: change to Cmaera* when stereo cameras are involved.
    Camera::CMonocularCam* pMonoCamera = NULL;
    
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    float f32 = 0.0f;
    float f33 = 0.0f;
    float fScale = 1.0f;
    
    CvMat* pMatRotation = cvCreateMat(3, 3, CV_64FC1);
    int iCurIndex = 0;
    
    for (int i = 0; i < iShowcaseCount; ++i)
    {
        pShowcase = g_pEngineCore->GetShowcaseWithIndex(i);
        iTrackableCount = pShowcase->GetTrackableCount();
        
        for (int j = 0; j < iTrackableCount; ++j)
        {
            pTrackable = pShowcase->GetTrackableByIndex(i);
            if (pTrackable->IsTracking())
            {
                if (TYPE_TRACKABLE_PL == pTrackable->GetType())
                {
                    // fill the status string
                    pMonoCamera = dynamic_cast<Repository::CPlanarTrackable*>(pTrackable)->GetCamera();
                    CvScalar Pos = pMonoCamera->GetPosition();
                    
                    pOutArr[iCurIndex].iTrackableID = iCurIndex + 1;
                    
                    pOutArr[iCurIndex].fCamPosX = -Pos.val[0] / fScale;
                    pOutArr[iCurIndex].fCamPosY = Pos.val[1] / fScale;
                    pOutArr[iCurIndex].fCamPosZ = Pos.val[2] / fScale;
                    
                    CvMat* pMatExtrin = pMonoCamera->GetExtrinsicMatrix();
                    
                    for(int y = 0; y < 3; ++y)
                    {
                        for(int x = 0; x < 3; ++x)
                        {
                            cvSetReal2D(pMatRotation, y, x, cvGetReal2D(pMatExtrin, y, x));
                        }
                    }
                    
                    float fCvt = 180.0f / CV_PI;
                    
                    x = atan2f(cvmGet(pMatRotation, 2, 1), cvmGet(pMatRotation, 2, 2)) * fCvt;
                    f32 = cvmGet(pMatRotation, 2, 1);
                    f33 = cvmGet(pMatRotation, 2, 2);
                    y = atan2f(-cvmGet(pMatRotation, 2, 0), powf(f32*f32 + f33*f33, 0.5f)) * fCvt;
                    z = atan2f(cvmGet(pMatRotation, 1, 0), cvmGet(pMatRotation, 0, 0)) * fCvt;
                    
                    pOutArr[iCurIndex].fCamRotX = -x;
                    pOutArr[iCurIndex].fCamRotY = -y;
                    pOutArr[iCurIndex].fCamRotZ = z;
                    
                    std::cout << "!!!!!! In SnowEggEngine AR State: \n " << pOutArr[iCurIndex].fCamPosX << "\n" << pOutArr[iCurIndex].fCamPosY << "\n" << pOutArr[iCurIndex].fCamPosZ << "\n" << pOutArr[iCurIndex].fCamRotX << "\n" << pOutArr[iCurIndex].fCamRotY << "\n" << pOutArr[iCurIndex].fCamRotZ << "\n";
                    
                    if (++iCurIndex >= *pElementsNum)
                    {
                        goto OUTSIDE;
                    }
                }
            }
        }
    }
    
OUTSIDE:
    *pElementsNum = iCurIndex;
    cvReleaseMat(&pMatRotation);
    
    return errorCode;
}

inline float SIGN(float x) {return (x >= 0.0f) ? +1.0f : -1.0f;}
inline float NORM(float a, float b, float c, float d) {return sqrt(a * a + b * b + c * c + d * d);}
void RotationMat2Quat(CvMat* pMatRotation, float& q0, float& q1, float& q2, float& q3)
{
    float r11 = cvmGet(pMatRotation, 0, 0);
    float r12 = cvmGet(pMatRotation, 0, 1);
    float r13 = cvmGet(pMatRotation, 0, 2);
    float r21 = cvmGet(pMatRotation, 1, 0);
    float r22 = cvmGet(pMatRotation, 1, 1);
    float r23 = cvmGet(pMatRotation, 1, 2);
    float r31 = cvmGet(pMatRotation, 2, 0);
    float r32 = cvmGet(pMatRotation, 2, 1);
    float r33 = cvmGet(pMatRotation, 2, 2);
    
    q0 = ( r11 + r22 + r33 + 1.0f) / 4.0f;
    q1 = ( r11 - r22 - r33 + 1.0f) / 4.0f;
    q2 = (-r11 + r22 - r33 + 1.0f) / 4.0f;
    q3 = (-r11 - r22 + r33 + 1.0f) / 4.0f;
    if (q0 < 0.0f) q0 = 0.0f;
    if (q1 < 0.0f) q1 = 0.0f;
    if (q2 < 0.0f) q2 = 0.0f;
    if (q3 < 0.0f) q3 = 0.0f;
    q0 = sqrt(q0);
    q1 = sqrt(q1);
    q2 = sqrt(q2);
    q3 = sqrt(q3);
    if (q0 >= q1 && q0 >= q2 && q0 >= q3)
    {
        q0 *= +1.0f;
        q1 *= SIGN(r32 - r23);
        q2 *= SIGN(r13 - r31);
        q3 *= SIGN(r21 - r12);
    }
    else if (q1 >= q0 && q1 >= q2 && q1 >= q3)
    {
        q0 *= SIGN(r32 - r23);
        q1 *= +1.0f;
        q2 *= SIGN(r21 + r12);
        q3 *= SIGN(r13 + r31);
    }
    else if (q2 >= q0 && q2 >= q1 && q2 >= q3)
    {
        q0 *= SIGN(r13 - r31);
        q1 *= SIGN(r21 + r12);
        q2 *= +1.0f;
        q3 *= SIGN(r32 + r23);
    }
    else if (q3 >= q0 && q3 >= q1 && q3 >= q2)
    {
        q0 *= SIGN(r21 - r12);
        q1 *= SIGN(r31 + r13);
        q2 *= SIGN(r32 + r23);
        q3 *= +1.0f;
    }
    else
    {
        printf("coding error\n");
    }
    float r = NORM(q0, q1, q2, q3);
    q0 /= r;
    q1 /= r;
    q2 /= r;
    q3 /= r;
}

int CSnowEggEngine::GetARStateQuaternion(SEBinding_Struct_ARStateQuatMap* pOutArr, int* pElementsNum)
{
    // the output array is a pre-generated array numelements long.
    //   we will modify num elements to match the amount of entries we will return
    if (NULL == g_pEngineCore || *pElementsNum <= 0)
    {
        // we at least need some data to modify!
        return API_ERR_GENERAL;
    }
    
    int errorCode = API_ERR_NONE;
    
    int iShowcaseCount = g_pEngineCore->GetShowcaseCount();
    int iTrackableCount = 0;
    Showcase::CShowcase* pShowcase = NULL;
    Repository::CTrackable* pTrackable = NULL;
    // TODO: change to Cmaera* when stereo cameras are involved.
    Camera::CMonocularCam* pMonoCamera = NULL;
    
    CvMat* pMatRotation = cvCreateMat(3, 3, CV_64FC1);
    int iCurIndex = 0;
    
    for (int i = 0; i < iShowcaseCount; ++i)
    {
        pShowcase = g_pEngineCore->GetShowcaseWithIndex(i);
        iTrackableCount = pShowcase->GetTrackableCount();
        
        for (int j = 0; j < iTrackableCount; ++j)
        {
            pTrackable = pShowcase->GetTrackableByIndex(i);
            if (pTrackable->IsTracking())
            {
                if (TYPE_TRACKABLE_PL == pTrackable->GetType())
                {
                    // fill the status string
                    pMonoCamera = dynamic_cast<Repository::CPlanarTrackable*>(pTrackable)->GetCamera();
                    CvScalar Pos = pMonoCamera->GetPosition();
                    
                    pOutArr[iCurIndex].iTrackableID = iCurIndex + 1;
                    
                    pOutArr[iCurIndex].fCamPosX = -Pos.val[0];
                    pOutArr[iCurIndex].fCamPosY = Pos.val[1];
                    pOutArr[iCurIndex].fCamPosZ = Pos.val[2];
                    
                    CvMat* pMatExtrin = pMonoCamera->GetExtrinsicMatrix();
                    
                    for(int y = 0; y < 3; ++y)
                    {
                        for(int x = 0; x < 3; ++x)
                        {
                            cvSetReal2D(pMatRotation, y, x, cvGetReal2D(pMatExtrin, y, x));
                        }
                    }
                    
                    RotationMat2Quat(pMatRotation, pOutArr[iCurIndex].fCamQuatA, pOutArr[iCurIndex].fCamQuatB, pOutArr[iCurIndex].fCamQuatC, pOutArr[iCurIndex].fCamQuatD);
                    
                    std::cout << "!!!!!! In SnowEggEngine AR Quan State: \n " << pOutArr[iCurIndex].fCamQuatA << "\n" << pOutArr[iCurIndex].fCamQuatB << "\n" << pOutArr[iCurIndex].fCamQuatC << "\n" << pOutArr[iCurIndex].fCamQuatD << "\n";
                    
                    if (++iCurIndex >= *pElementsNum)
                    {
                        goto OUTSIDE;
                    }
                }
            }
        }
    }
    
OUTSIDE:
    *pElementsNum = iCurIndex;
    cvReleaseMat(&pMatRotation);
    
    return errorCode;
}

char* CSnowEggEngine::GetCurState() const
{
    if (NULL == g_pEngineCore)
    {
        return NULL;
    }
    
    std::ostringstream ss;
    
    int iShowcaseCount = g_pEngineCore->GetShowcaseCount();
    int iTrackableCount = 0;
    int iTrackingCount = 0;
    Showcase::CShowcase* pShowcase = NULL;
    Repository::CTrackable* pTrackable = NULL;
    Camera::CMonocularCam* pMonoCamera = NULL;
    
    for (int i = 0; i < iShowcaseCount; ++i)
    {
        pShowcase = g_pEngineCore->GetShowcaseWithIndex(i);
        iTrackableCount = pShowcase->GetTrackableCount();
        
        for (int j = 0; j < iTrackableCount; ++j)
        {
            pTrackable = pShowcase->GetTrackableByIndex(i);
            if (pTrackable->IsTracking())
            {
                ++iTrackingCount;
                
                if (TYPE_TRACKABLE_PL == pTrackable->GetType())
                {
                    // fill the status string
                    pMonoCamera = dynamic_cast<Repository::CPlanarTrackable*>(pTrackable)->GetCamera();
                    
                    ss << "," << pTrackable->GetID();
                    
                    CvMat* pMatIntri = pMonoCamera->GetIntrinsicMatrix();
                    for (int y = 0; y < 3; ++y)
                    {
                        for (int x = 0; x < 3; ++x)
                        {
                            ss << "," << cvmGet(pMatIntri, y, x);
                        }
                    }
                    
                    CvMat* pMatExtri = pMonoCamera->GetExtrinsicMatrix();
                    for (int y = 0; y < 4; ++y)
                    {
                        for (int x = 0; x < 4; ++x)
                        {
                            ss << "," << cvmGet(pMatExtri, y, x);
                        }
                    }
                }
            }
        }
    }
    
    std::string strCurStatus = "";
    std::ostringstream ssWithNum;
    
    ssWithNum << iTrackingCount << ss.str();
    
    strCurStatus += ssWithNum.str();
    
    int iLength = static_cast<int>(strCurStatus.size());
    char* pStatus = new char[iLength + 1];
    
    memset(pStatus, 0, iLength + 1);
    memcpy(pStatus, strCurStatus.c_str(), iLength);
    
    return pStatus;
}

char* CSnowEggEngine::GetCurSimpleState() const
{
    if (NULL == g_pEngineCore)
    {
        return NULL;
    }
    
    std::ostringstream ss;
    
    int iShowcaseCount = g_pEngineCore->GetShowcaseCount();
    int iTrackableCount = 0;
    int iTrackingCount = 0;
    Showcase::CShowcase* pShowcase = NULL;
    Repository::CTrackable* pTrackable = NULL;
    Camera::CMonocularCam* pMonoCamera = NULL;
    
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;
    float f32 = 0.0f;
    float f33 = 0.0f;
    
    CvMat* pMatRotation = cvCreateMat(3, 3, CV_64FC1);
    
    for (int i = 0; i < iShowcaseCount; ++i)
    {
        pShowcase = g_pEngineCore->GetShowcaseWithIndex(i);
        iTrackableCount = pShowcase->GetTrackableCount();
        
        for (int j = 0; j < iTrackableCount; ++j)
        {
            pTrackable = pShowcase->GetTrackableByIndex(i);
            if (pTrackable->IsTracking())
            {
                ++iTrackingCount;
                
                if (TYPE_TRACKABLE_PL == pTrackable->GetType())
                {
                    // fill the status string
                    pMonoCamera = dynamic_cast<Repository::CPlanarTrackable*>(pTrackable)->GetCamera();
                    
                    ss << "," << pTrackable->GetID();
                    
                    CvScalar Pos = pMonoCamera->GetPosition();
                    ss << "," << Pos.val[0] << "," << Pos.val[1] << "," << Pos.val[2];
                    
                    CvMat* pMatExtrin = pMonoCamera->GetExtrinsicMatrix();
                    
                    for(int y = 0; y < 3; ++y)
                    {
                        for(int x = 0; x < 3; ++x)
                        {
                            cvSetReal2D(pMatRotation, y, x, cvGetReal2D(pMatExtrin, y, x));
                        }
                    }
                    
                    x = atan2f(cvmGet(pMatRotation, 2, 1), cvmGet(pMatRotation, 2, 2));
                    f32 = cvmGet(pMatRotation, 2, 1);
                    f33 = cvmGet(pMatRotation, 2, 2);
                    y = atan2f(-cvmGet(pMatRotation, 2, 0), powf(f32*f32 + f33*f33, 0.5f));
                    z = atan2f(cvmGet(pMatRotation, 1, 0), cvmGet(pMatRotation, 0, 0));
                    
                    ss << "," << x << "," << y << "," << z;
                }
            }
        }
    }
    
    cvReleaseMat(&pMatRotation);
    
    std::string strCurStatus = "";
    std::ostringstream ssWithNum;
    
    ssWithNum << iTrackingCount << ss.str();
    
    strCurStatus += ssWithNum.str();
    
    int iLength = static_cast<int>(strCurStatus.size());
    char* pStatus = new char[iLength + 1];
    
    memset(pStatus, 0, iLength + 1);
    memcpy(pStatus, strCurStatus.c_str(), iLength);
    
    return pStatus;
}

bool CSnowEggEngine::ReleaseBuffer(char* szBuf)
{
    if (NULL == szBuf)
    {
        return false;
    }
    
    SAFE_DELETE_ARR(szBuf);
    
    return true;
}

char* CSnowEggEngine::CreateShowcase(char* szShowcaseID, int iMaxTracking, int iShowcaseType)
{
    if (NULL == g_pEngineCore)
    {
        return NULL;
    }
    
    return g_pEngineCore->CreateShowcase(szShowcaseID, iShowcaseType, iMaxTracking);
}

bool CSnowEggEngine::ReleaseShowcase(char* szShowcaseID)
{
    if (NULL == g_pEngineCore)
    {
        return false;
    }
    
    return g_pEngineCore->ReleaseShowcase(szShowcaseID);
}

int CSnowEggEngine::GetShowcaseCount() const
{
    if (NULL == g_pEngineCore)
    {
        return 0;
    }
    
    return g_pEngineCore->GetShowcaseCount();
}

bool CSnowEggEngine::CreateMarker(char* szMarkerSource, int iMarkerType, char* szShowcaseID, char* szMarkerID, bool bAutoLoad)
{
    //TODO: option of Autoload
    if (NULL == g_pEngineCore || NULL == szShowcaseID || NULL == szMarkerSource)
    {
        return false;
    }
    
    Showcase::CShowcase* pShowcase = g_pEngineCore->GetShowcase(szShowcaseID);
    
    if (NULL == pShowcase)
    {
        return false;
    }
    
    return pShowcase->CreateTrackable(szMarkerSource, szMarkerID);
}

bool CSnowEggEngine::ReleaseMarker(char* szMarkerID, char* szShowcaseID)
{
    if (NULL == szMarkerID)
    {
        return false;
    }
    
    if (NULL == szShowcaseID)
    {
        int iShowcaseCount = g_pEngineCore->GetShowcaseCount();
        for (int i = 0; i < iShowcaseCount; ++i)
        {
            if (g_pEngineCore->GetShowcaseWithIndex(i)->ReleaseTrackable(szMarkerID))
            {
                return true;
            }
        }
    }
    else
    {
        return g_pEngineCore->GetShowcase(szShowcaseID)->ReleaseTrackable(szMarkerID);
    }
    
    return false;
}

bool CSnowEggEngine::LoadMarker(char* szMarkerID, char* szShowcaseID)
{
    if (NULL == szMarkerID)
    {
        return false;
    }
    
    if (NULL == szShowcaseID)
    {
        int iShowcaseCount = g_pEngineCore->GetShowcaseCount();
        for (int i = 0; i < iShowcaseCount; ++i)
        {
            if (g_pEngineCore->GetShowcaseWithIndex(i)->LoadTrackable(szMarkerID))
            {
                return true;
            }
        }
    }
    else
    {
        return g_pEngineCore->GetShowcase(szShowcaseID)->LoadTrackable(szMarkerID);
    }
    
    return false;
}

bool CSnowEggEngine::UnloadMarker(char* szMarkerID, char* szShowcaseID)
{
    if (NULL == szMarkerID)
    {
        return false;
    }
    
    if (NULL == szShowcaseID)
    {
        int iShowcaseCount = g_pEngineCore->GetShowcaseCount();
        for (int i = 0; i < iShowcaseCount; ++i)
        {
            if (g_pEngineCore->GetShowcaseWithIndex(i)->UnloadTrackable(szMarkerID))
            {
                return true;
            }
        }
    }
    else
    {
        return g_pEngineCore->GetShowcase(szShowcaseID)->UnloadTrackable(szMarkerID);
    }
    
    return false;
}

int CSnowEggEngine::GetMarkerCount(char* szShowcaseID) const
{
    if (NULL == szShowcaseID)
    {
        return -1;
    }
    
    Showcase::CShowcase* pShowcase = dynamic_cast<Showcase::CShowcase*>(g_pEngineCore->GetShowcase(szShowcaseID));
    
    if (NULL == pShowcase)
    {
        return -1;
    }
    
    return pShowcase->GetTrackableCount();
}


