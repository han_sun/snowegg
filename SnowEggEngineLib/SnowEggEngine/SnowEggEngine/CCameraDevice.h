//
//  CCameraDevice.h
//  SnowEgg
//
//  Created by Adrian Butt on 9/04/2015.
//  Copyright (c) 2015 Auggd. All rights reserved.
//

#ifndef __SnowEgg__CCameraDevice__
#define __SnowEgg__CCameraDevice__

#import "OpenCVIOSAdapt.h"

namespace SnowEgg
{
    class CCameraDevice;
    
    typedef void (*CALL_BACK_IMGPROC)(cv::Mat&, void* context);
    
    class CCameraDeviceImpl;
    
    class CCameraDevice
    {
    public:
        void    SetCamFeedUpdatedCallback(CALL_BACK_IMGPROC toCB, void* context);
        bool    InitCam();
        void    StartCam();
        void    SetTextureID(int toId, int texW, int texH, int texFormat);
        void    RenderCurrentCameraFrameToTexture();
        int     GetTextureID(bool createIfNone);
        int     GetTextureWidth();
        int     GetTextureHeight();
        int     GetRenderWidth();
        int     GetRenderHeight();
        
        CCameraDevice();
        ~CCameraDevice();
        
    private:
        CCameraDeviceImpl* impl;
    };
}

#endif /* defined(__SnowEgg__CCameraDevice__) */
