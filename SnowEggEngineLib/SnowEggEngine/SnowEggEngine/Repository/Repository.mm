//
//  Repository.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#include "Repository.h"
#include "../Base/Defines.h"
#include "PlanarTrackable.h"

using namespace SnowEgg;
using namespace Repository;

CRepository::CRepository()
{

}

CRepository::~CRepository()
{
    int iSize = static_cast<int>(m_vecListTrackable.size());
    
    if (iSize > 0)
    {
        for (int i = 0; i < iSize; ++i)
        {
            SAFE_DELETE(m_vecListTrackable[i]);
        }
        
        m_vecListTrackable.clear();
    }
}

bool CRepository::CreateTrackable(char* szRefSrc, char* szSpecified)
{
    if (NULL == szRefSrc)
    {
        return false;
    }
    
    CPlanarTrackable* pPlanarTrackable = new CPlanarTrackable();
    if (!pPlanarTrackable->Load(szRefSrc, szSpecified))
    {
        SAFE_DELETE(pPlanarTrackable);
        return false;
    }
    
    m_vecListTrackable.push_back(pPlanarTrackable);
    
    return true;
}

CTrackable* CRepository::GetTrackableByName(char* szID) const
{
    int iSize = static_cast<int>(m_vecListTrackable.size());
    
    for (int i = 0; i < iSize; ++i)
    {
        if (0 == strcmp(m_vecListTrackable[i]->GetID(), szID))
        {
            return m_vecListTrackable[i];
        }
    }
    
    return NULL;
}

std::vector<CTrackable*>& CRepository::GetTrackableListRef()
{
    return m_vecListTrackable;
}
