//
//  Trackable.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#include <string.h>
#include "Trackable.h"

using namespace SnowEgg;
using namespace Repository;

CTrackable::CTrackable()
{
    m_bIsTracking = false;
}

CTrackable::~CTrackable()
{

}

bool CTrackable::IsTracking()
{
    return m_bIsTracking;
}

void CTrackable::SetTracking(bool bIsTracking)
{
    m_bIsTracking = bIsTracking;
}

char* CTrackable::GetID()
{
    return m_szID;
}

void CTrackable::SetID(const char* szID)
{
    if (NULL == szID)
    {
        return;
    }
    
    int iLength = static_cast<int>(strlen(szID));
    memset(m_szID, 0, iLength + 1);
    memcpy(m_szID, szID, iLength);
}