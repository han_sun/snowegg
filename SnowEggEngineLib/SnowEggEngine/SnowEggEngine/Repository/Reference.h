//
//  Reference.h
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#ifndef __REFERENCE_H__
#define __REFERENCE_H__

#include <stdio.h>
#include <string>

namespace SnowEgg
{
    namespace Repository
    {
        class CReference
        {
        public:
            CReference();
            virtual ~CReference();
            
        public:
            virtual void SetReferenceID(char* szRefID) = 0;
            virtual const char* GetReferenceID() const = 0;
            virtual bool Load() = 0;
            virtual void Unload() = 0;
            
        protected:
            std::string m_strRefID;
        };
    }
}

#endif /* defined(__REFERENCE_H__) */
