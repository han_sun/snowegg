//
//  PlanarReference.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//
#import <Foundation/Foundation.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include "../Base/Defines.h"
#include "ReferencePB.pb.h"
#include "PlanarReference.h"

using namespace SnowEgg;
using namespace Repository;

CPlanarReference::CPlanarReference()
{
    m_pListFP = new std::vector<Feature::CFeaturePoint*>();
}

CPlanarReference::~CPlanarReference()
{
    this->Unload();
    
    SAFE_DELETE(m_pListFP);
}

void CPlanarReference::SetReferenceID(char* szRefID)
{
    if (NULL != szRefID)
    {
        m_strRefID = szRefID;
    }
}

const char* CPlanarReference::GetReferenceID() const
{
    return m_strRefID.c_str();
}

bool CPlanarReference::Load()
{
    if ("" == m_strRefID)
    {
        return false;
    }
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithCString:m_strRefID.c_str() encoding:[NSString defaultCStringEncoding]] ofType:@"agd"];
    
    std::cout << "!!!!!!!!!In SnowEggEngine, going to load marker at " << m_strRefID << std::endl;
    
    RefFeaturePointList_RefFeaturePoint_Color4 color4;
    RefFeaturePointList_RefFeaturePoint_Vector3 point3;
    RefFeaturePointList listFp1;
    
    std::fstream input(filePath.UTF8String, std::ios::in | std::ios::binary);
   
    if (!listFp1.ParseFromIstream(&input))
    {
        std::cerr << "Failed to parse marker." << std::endl;
    }
    
//    m_vecListFP.clear();
    int iFpSize = listFp1.fp_size();
    
    for (int i = 0; i < iFpSize; ++i)
    {
        Feature::CFeaturePoint* pTempFp = new Feature::CFeaturePoint();
        
        point3 = listFp1.fp(i).point();
        CVector3f tempPoint;
        tempPoint.x = point3.x();
        tempPoint.y = point3.y();
        tempPoint.z = point3.z();
        pTempFp->SetPosition(tempPoint);
        
        color4 = listFp1.fp(i).color();
        CvScalar tempColor;
        tempColor.val[0] = color4.r();
        tempColor.val[1] = color4.g();
        tempColor.val[2] = color4.b();
        tempColor.val[3] = color4.a();
        pTempFp->SetColor(tempColor);
        
        pTempFp->SetTrackableID(listFp1.fp(i).objectid());
        
        pTempFp->SetSize(listFp1.fp(i).size());
        
        pTempFp->SetDir(listFp1.fp(i).dir());
        
        pTempFp->SetRepoIndex(listFp1.fp(i).repositoryid());
        
        pTempFp->SetDesDim(listFp1.fp(i).descriptordim());
        
        int iDescSize = listFp1.fp(i).featuredesc_size();
        
        for (int j = 0; j < iDescSize; ++j)
        {
            pTempFp->GetDescriptors()[j] = listFp1.fp(i).featuredesc(j);
        }
        
        m_pListFP->push_back(pTempFp);
    }
    
    return true;
}

void CPlanarReference::Unload()
{
    int iSize = static_cast<int>(m_pListFP->size());
    
    for (int i = 0; i < iSize; ++i)
    {
        SAFE_DELETE((*m_pListFP)[i]);
    }
    
    m_pListFP->clear();
}

std::vector<Feature::CFeaturePoint*>* CPlanarReference::GetFPList()
{
    return m_pListFP;
}