//
//  PlanarTrackable.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 11/03/2015.
//
//

#include "PlanarTrackable.h"
#include "../Matcher/2DMatchedPair.h"

using namespace SnowEgg;
using namespace Repository;

CPlanarTrackable::CPlanarTrackable()
{
    m_pReference = new CPlanarReference();
    m_pMatcher = new Matcher::C2DMatcher();
    m_pVecListPair = new std::vector<Matcher::C2DMatchedPair*>();
    m_pCamera = new Camera::CMonocularCam();
    m_pCamera->SetIntrinsicMatrix(WIDTH_CAM*1.2, WIDTH_CAM*1.2, WIDTH_CAM/2.0, HEIGHT_CAM/2.0);
    m_pCamera->SetDistortionCoefficients(0.0f, 0.0f, 0.0f, 0.0f);
    m_iTimeSteps = 0;
    memset(m_szID, 0, MAX_LENGTH_ID + 1);
}

CPlanarTrackable::~CPlanarTrackable()
{    
    SAFE_DELETE(m_pReference);
    SAFE_DELETE(m_pMatcher);
    
    if (NULL != m_pVecListPair)
    {
        int iSize = static_cast<int>(m_pVecListPair->size());
        
        for (int i = 0; i < iSize; ++i)
        {
            SAFE_DELETE((*m_pVecListPair)[i]);
        }
        
        SAFE_DELETE(m_pVecListPair);
    }
    
    SAFE_DELETE(m_pCamera);
}

bool CPlanarTrackable::Load(char *szRefSrc, char* szSpecified)
{
    if (NULL == szRefSrc)
    {
        return false;
    }
    
    m_pReference->SetReferenceID(szRefSrc);
    if (!m_pReference->Load())
    {
        return false;
    }
    
    if (!m_pMatcher->Train(m_pReference->GetFPList()))
    {
        m_pReference->Unload();
        return false;
    }
    
    if (NULL != szSpecified)
    {
        memset(m_szID, 0, MAX_LENGTH_ID);
        int iLength = static_cast<int>(strlen(szSpecified));
        
        if (iLength > MAX_LENGTH_ID)
        {
            return false;
        }
        
        memcpy(m_szID, szSpecified, iLength);
    }
    
    return true;
}

bool CPlanarTrackable::LoadContent()
{
    return m_pReference->Load();
}

void CPlanarTrackable::UnloadContent()
{
    m_pReference->Unload();
}

bool CPlanarTrackable::Match(std::vector<Feature::CFeaturePoint*>* pVecSceFP)
{
    int iSize = static_cast<int>(pVecSceFP->size());
    
    double dDistance = 0.0f;
    int iMatchedIndex = -1;
    for (int i = 0; i < iSize; ++i)
    {
        iMatchedIndex = m_pMatcher->Match((*pVecSceFP)[i], &dDistance);
        
        if (iMatchedIndex >= 0)
        {
            Matcher::C2DMatchedPair* pNewPair = new Matcher::C2DMatchedPair();
            pNewPair->SetDistance(dDistance);
            pNewPair->SetRefFeaturePoint((*m_pReference->GetFPList())[iMatchedIndex]);
            Feature::CFeaturePoint* pCopySceFP = new Feature::CFeaturePoint();
            *pCopySceFP = *(*pVecSceFP)[i];
            pNewPair->SetSceFeaturePoint(pCopySceFP);
            
            m_pVecListPair->push_back(pNewPair);
        }
    }
    
    return true;
}

CPlanarReference* CPlanarTrackable::GetReference() const
{
    return m_pReference;
}

Matcher::C2DMatcher* CPlanarTrackable::GetMatcher() const
{
    return m_pMatcher;
}

std::vector<Matcher::C2DMatchedPair*>* CPlanarTrackable::GetMatchedList()
{
    return m_pVecListPair;
}

Camera::CMonocularCam* CPlanarTrackable::GetCamera() const
{
    return m_pCamera;
}

int CPlanarTrackable::GetTimeSteps() const
{
    return m_iTimeSteps;
}

void CPlanarTrackable::SetTimeSteps(int iTimeStep)
{
    m_iTimeSteps = iTimeStep;
}

int CPlanarTrackable::GetType() const
{
    return TYPE_TRACKABLE_PL;
}

void CPlanarTrackable::ClearMatchedList()
{
    int iSize = static_cast<int>(m_pVecListPair->size());
    
    for (int i = 0; i < iSize; ++i)
    {
        SAFE_DELETE((*m_pVecListPair)[i]);
    }
    
    m_pVecListPair->clear();
}