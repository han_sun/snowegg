//
//  PlanarReference.h
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#ifndef __PLANARREFERENCE_H__
#define __PLANARREFERENCE_H__

#include <stdio.h>
#include <vector>
#include "Reference.h"
#include "../Feature/FeaturePoint.h"

namespace SnowEgg
{
    namespace Repository
    {
        class CPlanarReference : virtual public CReference
        {
        public:
            CPlanarReference();
            virtual ~CPlanarReference();
            
        public:
            virtual void SetReferenceID(char* szRefID);
            virtual const char* GetReferenceID() const;
            virtual bool Load();
            virtual void Unload();
            
        public:
            std::vector<Feature::CFeaturePoint*>* GetFPList();
            
        protected:
            std::vector<Feature::CFeaturePoint*>* m_pListFP;
        };
    }
}

#endif /* defined(__PLANARREFERENCE_H__) */
