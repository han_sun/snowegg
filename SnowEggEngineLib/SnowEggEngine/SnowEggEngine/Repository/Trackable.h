//
//  Trackable.h
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#ifndef __TRACKABLE_H__
#define __TRACKABLE_H__

#include <stdio.h>
#include "../Base/Defines.h"

namespace SnowEgg
{
    namespace Repository
    {
        class CTrackable
        {
        public:
            CTrackable();
            virtual ~CTrackable();
            
        public:
            virtual bool Load(char* szRefSrc, char* szSpecified) = 0; // load ref, train matcher
            virtual bool LoadContent() = 0; // need to call load first
            virtual void UnloadContent() = 0;
            
        public:
            virtual int GetType() const = 0;
            
        public:
            bool IsTracking();
            void SetTracking(bool bIsTracking);
            
        public:
            // TODO: need to abstract camera to this level
            //virtual Camera::CCamera* GetCamera() const = 0;
            
        public:
            char* GetID();
            void SetID(const char* szID);
            
        protected:
            char m_szID[MAX_LENGTH_ID + 1];
            bool m_bIsTracking;
        };
    }
}

#endif /* defined(__TRACKABLE_H__) */
