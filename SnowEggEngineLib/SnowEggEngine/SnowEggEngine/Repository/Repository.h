//
//  Repository.h
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#ifndef __REPOSITORY_H__
#define __REPOSITORY_H__

#include <stdio.h>
#include <vector>
#include "Trackable.h"

namespace SnowEgg
{
    namespace Repository
    {
        class CRepository
        {
        public:
            CRepository();
            ~CRepository();
            
        public:
            bool CreateTrackable(char* szRefSrc, char* szSpecified = NULL);
            CTrackable* GetTrackableByName(char* szID) const;
            std::vector<CTrackable*>& GetTrackableListRef();
            
        protected:
            std::vector<CTrackable*> m_vecListTrackable;
        };
    }
}

#endif /* defined(__REPOSITORY_H__) */
