//
//  PlanarTrackable.h
//  SnowEggEngine
//
//  Created by Han Sun on 11/03/2015.
//
//

#ifndef __PLANARTRACKABLE_H__
#define __PLANARTRACKABLE_H__

#include <stdio.h>
#include <vector>
#include "PlanarReference.h"
#include "../Matcher/2DMatcher.h"
#include "../Matcher/2DMatchedPair.h"
#include "Trackable.h"
#include "../Camera/MonocularCam.h"

namespace SnowEgg
{
    namespace Repository
    {
        class CPlanarTrackable : virtual public CTrackable
        {
        public:
            CPlanarTrackable();
            virtual ~CPlanarTrackable();
            
        public:
            virtual bool Load(char* szRefSrc, char* szSpecified);
            virtual bool LoadContent();
            virtual void UnloadContent();
            
        public:
            virtual int GetType() const;
            
        public:
            bool Match(std::vector<Feature::CFeaturePoint*>* pVecSceFP);
            void ClearMatchedList();
            
        public:
            CPlanarReference* GetReference() const;
            Matcher::C2DMatcher* GetMatcher() const;
            std::vector<Matcher::C2DMatchedPair*>* GetMatchedList();
            Camera::CMonocularCam* GetCamera() const;
            
        public:
            int GetTimeSteps() const;
            void SetTimeSteps(int iTimeStep);
            
        protected:
            CPlanarReference* m_pReference;
            Matcher::C2DMatcher* m_pMatcher;
            std::vector<Matcher::C2DMatchedPair*>* m_pVecListPair;
            Camera::CMonocularCam* m_pCamera;
            
        protected:
            int m_iTimeSteps;
        };
    }
}

#endif /* defined(__PLANARTRACKABLE_H__) */
