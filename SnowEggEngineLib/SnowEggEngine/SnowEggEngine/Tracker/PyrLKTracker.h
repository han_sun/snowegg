//
//  PyrLKTracker.h
//  SnowEggEngine
//
//  Created by Han Sun on 5/03/2015.
//
//

#ifndef __PYRLKTRACKER_H__
#define __PYRLKTRACKER_H__

#include "../Base/Defines.h"
#include "Tracker.h"
#include "../Matcher/2DMatchedPair.h"

namespace SnowEgg
{
    namespace Tracker
    {
        class CPyrLKTracker : virtual public CTracker
        {
        public:
            CPyrLKTracker();
            virtual ~CPyrLKTracker();
            
        public:
            virtual bool Track(IplImage* pPrevGray, IplImage* pCurrGray, std::vector<Matcher::C2DMatchedPair*>* pListPrevPoints);
            
        protected:
            int m_iImgWidth;
            int m_iImgHeight;
            CvSize m_winSize;
            
        protected:
            CvPoint2D32f m_trackingPoints[NUM_MAX_TRACK];
            CvPoint2D32f m_trackedPoints[NUM_MAX_TRACK];
            float m_fErrorPoints[NUM_MAX_TRACK];
            
        protected:
            IplImage* m_pPrevPyr;
            IplImage* m_pCurrPyr;
            int m_iLvPyr;
            
        protected:
            CvTermCriteria m_termCrit;
            
        protected:
            char m_szStates[NUM_MAX_TRACK];
        };
    }
}

#endif /* defined(__PYRLKTRACKER_H__) */
