//
//  PyrLKTracker.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 5/03/2015.
//
//

#include "../Base/OpenCVIOSAdapt.h"
#include "PyrLKTracker.h"

using namespace SnowEgg;
using namespace Tracker;

CPyrLKTracker::CPyrLKTracker()
{
    m_iImgWidth = WIDTH_TRACK_IMG;
    m_iImgHeight = HEIGHT_TRACK_IMG;
    m_winSize.width = SIZE_TRACK_WINDOW;
    m_winSize.height = SIZE_TRACK_WINDOW;
    
    for (int i = 0; i < NUM_MAX_TRACK; ++i)
    {
        m_fErrorPoints[i] = 0.0f;
        m_szStates[i] = 0;
    }
    
    m_pPrevPyr = cvCreateImage(m_winSize, IPL_DEPTH_8U, 1);
    m_pCurrPyr = cvCreateImage(m_winSize, IPL_DEPTH_8U, 1);
    
    m_iLvPyr = NUM_LV_PYR;
    
    m_termCrit = cvTermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, NUM_MAX_TRACKITER, EPS_TRACK);
}

CPyrLKTracker::~CPyrLKTracker()
{
    if (NULL != m_pPrevPyr)
    {
        cvReleaseImage(&m_pPrevPyr);
        m_pPrevPyr = NULL;
    }
    
    if (NULL != m_pCurrPyr)
    {
        cvReleaseImage(&m_pCurrPyr);
        m_pCurrPyr = NULL;
    }
}

bool CPyrLKTracker::Track(IplImage* pPrevGray, IplImage* pCurrGray, std::vector<Matcher::C2DMatchedPair*>* pListPrevPoints)
{
    int iNumPoints = MIN(static_cast<int>(pListPrevPoints->size()), NUM_MAX_TRACK);
    
    if (iNumPoints <= 0)
    {
        return false;
    }
    
    IplImage* pTempPrev = cvCloneImage(pPrevGray);
    IplImage* pTempCurr = cvCloneImage(pCurrGray);
    
    cvSmooth(pTempPrev, pTempPrev, CV_GAUSSIAN, 3, 3);
    cvSmooth(pTempCurr, pTempCurr, CV_GAUSSIAN, 3, 3);
    
    for(int i = 0; i < iNumPoints; ++i)
    {
        m_trackingPoints[i] = cvPoint2D32f((*pListPrevPoints)[i]->GetSceFeaturePoint()->GetPosition().x,
                                           (*pListPrevPoints)[i]->GetSceFeaturePoint()->GetPosition().y);
    }
    
    cvCalcOpticalFlowPyrLK(pTempPrev, pTempCurr, m_pPrevPyr, m_pCurrPyr,
                           m_trackingPoints, m_trackedPoints, iNumPoints,
                           m_winSize, m_iLvPyr, m_szStates, m_fErrorPoints,
                           m_termCrit, 0);
    
    cvReleaseImage(&pTempPrev);
    cvReleaseImage(&pTempCurr);
    
    for(int i = 0; i < iNumPoints; ++i)
    {
        (*pListPrevPoints)[i]->SetIsTracking(false);
        if(0 != m_szStates[i])
        {
            (*pListPrevPoints)[i]->SetIsTracking(true);
            (*pListPrevPoints)[i]->GetSceFeaturePoint()->SetPosition(CVector3f(m_trackedPoints[i].x,
                                                        m_trackedPoints[i].y,
                                                        1.0f));
        }
    }
    
    return true;
}