//
//  Tracker.h
//  SnowEggEngine
//
//  Created by Han Sun on 5/03/2015.
//
//

#ifndef __TRACKER_H__
#define __TRACKER_H__

#include "../Base/OpenCVIOSAdapt.h"
#include "../Feature/FeaturePoint.h"

namespace SnowEgg
{
    namespace Tracker
    {
        class CTracker
        {
        public:
            CTracker();
            virtual ~CTracker();
            
        public:
            //virtual bool Track(IplImage* pPrevGray, IplImage* pCurrGray, std::vector<Feature::CFeaturePoint*>* pListPrevPoints, char* szStates) = 0;
        };
    }
}

#endif /* defined(__TRACKER_H__) */
