//
//  EngineCore.h
//  SnowEgg
//
//  Created by Han Sun on 24/02/2015.
//
//

#ifndef __ENGINECORE_H__
#define __ENGINECORE_H__

#include <vector>
#include "CCameraDevice.h"
#include "Showcase/Showcase.h"

namespace SnowEgg
{
    typedef void (*CALL_BACK_DIDACC)(float x, float y, float z);
    
    class CEngineCore
    {
    public:
        static CEngineCore* GetInstance();
        
    public:
        bool InitEngine();
        void StartEngine();
        void StopEngine();
        CCameraDevice* GetCameraDevice() const;
        
    public:
        // Return NULL if fail
        char* CreateShowcase(const char* szShowcaseID, int iType, int iMaxTracking);
        bool ReleaseShowcase(const char* szShowcaseID);
        bool ReleaseAllShowcase();
        // Return NULL if cannot find the specified ID
        Showcase::CShowcase* GetShowcase(const char* szShowcaseID);
        // Return NULL if cannot find the specified index
        Showcase::CShowcase* GetShowcaseWithIndex(int iIndex);
        int GetShowcaseCount();
        
    public:
        void StartTracking();
        void StopTracking();
        
    private:
        CEngineCore();
        // Don't implement this forever
        CEngineCore(CEngineCore const&);
        // Don't implement this forever
        void operator=(CEngineCore const&);
        
    private:
        void ProcessImage(cv::Mat& image);
        static void CallBackImgProc(cv::Mat& image, void* context);
        void CallBackDidAccelerate(float x, float y, float z);
        
    private:
        CCameraDevice* m_pCamDevice;
        std::vector<Showcase::CShowcase*> m_vecShowcases;
        bool m_bStartTracking;
    };
}

#endif /* defined(__ENGINECORE_H__) */

