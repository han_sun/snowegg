//
//  Camera.h
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "../Base/OpenCVIOSAdapt.h"

namespace SnowEgg
{
    namespace Camera
    {
        class CCamera
        {
        public:
            CCamera();
            virtual ~CCamera();
            
        public:
            virtual int GetType() const = 0;
            
        public:
            virtual double* GetIntrinsicParameters() = 0;
            virtual CvMat* GetIntrinsicMatrix() = 0;
            virtual CvMat* GetDistortionCoefficients() = 0;
            virtual CvMat* GetExtrinsicMatrix() = 0;
            virtual void ConvertExtrinsicParameter(const CvMat* pVecRotation, CvMat* pVecTranslation) = 0;
            
        public:
            virtual CvScalar GetPosition() = 0;
            virtual CvScalar GetLookAt() = 0;
            virtual CvScalar GetUpPoint() = 0;
            virtual CvScalar GetRightPoint() = 0;
            
        public:
            virtual void CvtWorld2Cam(CvMat* pVec4Cam, CvMat* pVec4World) = 0;
            virtual void CvtCam2Img(CvMat* pVec3Img, CvMat* pVec3Cam) = 0;
            virtual void CvtWorld2Img(CvMat* pVec3Img, CvMat* pVec4World) = 0;
            virtual CvPoint CvtWorld2Img(double x, double y, double z) = 0;
            virtual void CvtCam2World(CvMat* pVec3World, CvMat* pVec3Cam) = 0;
            virtual CvScalar CvtCam2World(double x, double y, double z) = 0;
            virtual void CvtImg2Cam(CvMat* pVec3Cam, CvMat* pVec3Img, double dDepth) = 0;
            virtual void CvtImg2World(CvMat* pVec3World, CvMat* pVec3Img, double dDepth) = 0;
            virtual CvPoint2D64f CvtImg2World(double x, double y, double dDepth = 0.0f) = 0;
            virtual void CvtVecCam2World(CvMat* pVec3World, CvMat* pVec3Cam) = 0;
            virtual CvScalar CvtVecCam2World(double x, double y, double z) = 0;

        public:
            virtual void DrawInfomation(IplImage* pColorImage, double dSize) = 0;
        };
    }
}

#endif /* defined(__CAMERA_H__) */
