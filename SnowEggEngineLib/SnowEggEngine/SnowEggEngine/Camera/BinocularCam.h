//
//  BinocularCam.h
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#ifndef __BINOCULARCAM_H__
#define __BINOCULARCAM_H__

#include "Camera.h"

namespace SnowEgg
{
    namespace Camera
    {
        class CBinocularCam : virtual public CCamera
        {
        public:
            CBinocularCam();
            virtual ~CBinocularCam();
            
        public:
            virtual int GetType() const;
            
        public:
            virtual void operator = (CCamera &right);
            
        public:
            virtual double* GetParameters();
            virtual CvMat* GetIntrinsicMatrix();
            virtual CvMat* GetDistortionCoefficients();
            virtual CvMat* GetExtrinsicMatrix();
            virtual void ConvertExtrinsicParameter(CvMat* pVecRotation, CvMat* pVecTranslation);
            
        public:
            virtual CvScalar GetPosition();
            virtual CvScalar GetLookAt();
            virtual CvScalar GetUpPoint();
            virtual CvScalar GetRightPoint();
            
        public:
            virtual void CvtWorldToCam(CvMat* pVec4Cam, CvMat* pVec4World);
            virtual void CvtCam2Img(CvMat* pVec3Img, CvMat* pVec3Cam);
            virtual void CvtWorld2Img(CvMat* pVec3Img, CvMat* pVec4World);
            virtual CvPoint CvtWorld2Img(double x, double y, double z);
            virtual void CvtCam2World(CvMat* pVec3World, CvMat* pVec3Cam);
            virtual CvScalar CvtCam2World(double x, double y, double z);
            virtual void CvtImg2Cam(CvMat* pVec3Cam, CvMat* pVec3Img, double dDepth);
            virtual void CvtImg2World(CvMat* pVec3World, CvMat* pVec3Img, double dDepth);
            virtual CvPoint2D64f CvtImg2World(double x, double y, double dDepth = 0.0f);
            virtual void CvtVecCam2World(CvMat* pVec3World, CvMat* pVec3Cam);
            virtual CvScalar CvtVecCam2World(double x, double y, double z);
            
        public:
            virtual void DrawInfomation(IplImage* pColorImage, double dSize);
        };
    }
}

#endif /* defined(__BINOCULARCAM_H__) */
