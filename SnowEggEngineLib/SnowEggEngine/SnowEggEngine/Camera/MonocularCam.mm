//
//  MonocularCam.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#include "MonocularCam.h"
#include "../Base/Matrix33f.h"
#include "../Base/Defines.h"

using namespace SnowEgg;
using namespace Camera;

CMonocularCam::CMonocularCam()
{
    m_pMatIntr = cvCreateMat(3, 3, CV_64FC1);
    cvSetZero(m_pMatIntr);
    
    m_pMatExtr = cvCreateMat(4, 4, CV_64FC1);
    cvSetZero(m_pMatExtr);
    for (int i = 0; i < 4; ++i)
    {
        cvSetReal2D(m_pMatExtr, i, i, 1.0f);
    }
    
    m_pMatDisCoeffs = cvCreateMat(4, 1, CV_64FC1);
    cvSetZero(m_pMatDisCoeffs);
}

CMonocularCam::~CMonocularCam()
{
    if (NULL != m_pMatIntr)
    {
        cvReleaseMat(&m_pMatIntr);
        m_pMatIntr = NULL;
    }
    
    if (NULL != m_pMatExtr)
    {
        cvReleaseMat(&m_pMatExtr);
        m_pMatExtr = NULL;
    }
    
    if (NULL != m_pMatDisCoeffs)
    {
        cvReleaseMat(&m_pMatDisCoeffs);
        m_pMatDisCoeffs = NULL;
    }
}

int CMonocularCam::GetType() const
{
    return TYPE_CAM_MONO;
}

void CMonocularCam::operator = (CMonocularCam &right)
{
    this->SetIntrinsicMatrix(right.GetIntrinsicParameters()[0],
                             right.GetIntrinsicParameters()[1],
                             right.GetIntrinsicParameters()[2],
                             right.GetIntrinsicParameters()[3]);
    
    this->SetDistortionCoefficients(right.GetIntrinsicParameters()[4],
                                    right.GetIntrinsicParameters()[5],
                                    right.GetIntrinsicParameters()[6],
                                    right.GetIntrinsicParameters()[7]);
    
    this->SetExtrinsicMatrix(*right.GetExtrinsicMatrix());
}

double* CMonocularCam::GetIntrinsicParameters()
{
    return m_dIntrinsicParams;
}

CvMat* CMonocularCam::GetIntrinsicMatrix()
{
    return m_pMatIntr;
}

CvMat* CMonocularCam::GetDistortionCoefficients()
{
    return m_pMatDisCoeffs;
}

CvMat* CMonocularCam::GetExtrinsicMatrix()
{
    return m_pMatExtr;
}

void CMonocularCam::ConvertExtrinsicParameter(const CvMat* pVecRotation, CvMat* pVecTranslation)
{
    if (NULL == pVecRotation || NULL == pVecTranslation)
    {
        return;
    }
    
    CvMat* pMatRotation = cvCreateMat(3, 3, CV_64FC1);
    cvSetZero(pMatRotation);

    for(int i = 0; i < 3; ++i)
    {
        cvSetReal2D(pMatRotation, i, 3, cvGetReal1D(pVecTranslation, i));
    }
    
    cvRodrigues2(pVecRotation, pMatRotation);
    
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            cvSetReal2D(&m_pMatExtr, i, j, cvGetReal2D(pMatRotation, i, j));
        }
    }
    
    cvSetReal2D(m_pMatExtr, 3, 3, 1.0f);
    
    cvReleaseMat(&pMatRotation);
}

void CMonocularCam::SetIntrinsicMatrix(double fx, double fy, double cx, double cy)
{
    cvSetZero(m_pMatIntr);
    cvmSet(m_pMatIntr, 0, 0, fx);
    cvmSet(m_pMatIntr, 1, 1, fy);
    cvmSet(m_pMatIntr, 0, 2, cx);
    cvmSet(m_pMatIntr, 1, 2, cy);
    cvmSet(m_pMatIntr, 2, 2, 1);
    
    m_dIntrinsicParams[0] = fx;
    m_dIntrinsicParams[1] = fy;
    m_dIntrinsicParams[2] = cx;
    m_dIntrinsicParams[3] = cy;
}

void CMonocularCam::SetDistortionCoefficients(double k1, double k2, double k3, double k4)
{
    cvSetZero(m_pMatDisCoeffs);
    cvmSet(m_pMatDisCoeffs, 0, 0, k1);
    cvmSet(m_pMatDisCoeffs, 1, 0, k2);
    cvmSet(m_pMatDisCoeffs, 2, 0, k3);
    cvmSet(m_pMatDisCoeffs, 3, 0, k4);
    
    m_dIntrinsicParams[4] = k1;
    m_dIntrinsicParams[5] = k2;
    m_dIntrinsicParams[6] = k3;
    m_dIntrinsicParams[7] = k4;
}

void CMonocularCam::SetExtrinsicMatrix(CvMat& extMat)
{
    cvCopy(&extMat, m_pMatExtr);
}

CvScalar CMonocularCam::GetPosition()
{
    CvScalar vecPosition;
    
    CMatrix33f matRotation;
    matRotation.m[0][0] = cvGetReal2D(m_pMatExtr, 0, 0);
    matRotation.m[0][1] = cvGetReal2D(m_pMatExtr, 1, 0);
    matRotation.m[0][2] = cvGetReal2D(m_pMatExtr, 2, 0);
    
    matRotation.m[1][0] = cvGetReal2D(m_pMatExtr, 0, 1);
    matRotation.m[1][1] = cvGetReal2D(m_pMatExtr, 1, 1);
    matRotation.m[1][2] = cvGetReal2D(m_pMatExtr, 2, 1);
    
    matRotation.m[2][0] = cvGetReal2D(m_pMatExtr, 0, 2);
    matRotation.m[2][1] = cvGetReal2D(m_pMatExtr, 1, 2);
    matRotation.m[2][2] = cvGetReal2D(m_pMatExtr, 2, 2);
    
    CVector3f vecTranslation;
    vecTranslation.x = cvGetReal2D(m_pMatExtr, 0, 3);
    vecTranslation.y = cvGetReal2D(m_pMatExtr, 1, 3);
    vecTranslation.z = cvGetReal2D(m_pMatExtr, 2, 3);
    
    vecTranslation = -vecTranslation;
    CVector3f vecTemp = matRotation * vecTranslation;
    
    vecPosition.val[0] = vecTemp.x;
    vecPosition.val[1] = vecTemp.y;
    vecPosition.val[2] = vecTemp.z;

    return vecPosition;
}

CvScalar CMonocularCam::GetLookAt()
{
    CvScalar vecLookAt = this->CvtCam2World(0.0f, 0.0f, 1.0f);
    return vecLookAt;
}

CvScalar CMonocularCam::GetUpPoint()
{
    CvScalar vecUp = this->CvtCam2World(0.0f, -1.0f, 0.0f);
    return vecUp;
}

CvScalar CMonocularCam::GetRightPoint()
{
    CvScalar vecRight = this->CvtCam2World(1.0f, 0.0f, 0.0f);
    return vecRight;
}

void CMonocularCam::CvtWorld2Cam(CvMat* pVec4Cam, CvMat* pVec4World)
{
    if (NULL == pVec4Cam || NULL == pVec4World)
    {
        return;
    }
    
    cvMatMul(m_pMatExtr, pVec4World, pVec4Cam);
}

void CMonocularCam::CvtCam2Img(CvMat* pVec3Img, CvMat* pVec3Cam)
{
    if (NULL == pVec3Img || NULL == pVec3Cam)
    {
        return;
    }
    
    cvMatMul(m_pMatIntr, pVec3Cam, pVec3Img);
}

void CMonocularCam::CvtWorld2Img(CvMat* pVec3Img, CvMat* pVec4World)
{
    if (NULL == pVec3Img || NULL == pVec4World)
    {
        return;
    }
    
    CvMat* pMatCamCoordPos = cvCreateMat(4, 1, CV_64FC1);
    this->CvtWorld2Cam(pMatCamCoordPos, pVec4World);
    
    CvMat* pMat3DCamPos = cvCreateMat(3, 1, CV_64FC1);
    cvSetReal1D(pMat3DCamPos, 0, cvGetReal1D(pMatCamCoordPos, 0));
    cvSetReal1D(pMat3DCamPos, 1, cvGetReal1D(pMatCamCoordPos, 1));
    cvSetReal1D(pMat3DCamPos, 2, cvGetReal1D(pMatCamCoordPos, 2));
    
    CvtCam2Img(pVec3Img, pMat3DCamPos);
    
    cvSetReal1D(pVec3Img, 0, cvGetReal1D(pVec3Img, 0));
    cvSetReal1D(pVec3Img, 1, cvGetReal1D(pVec3Img, 1));
    cvSetReal1D(pVec3Img, 2, cvGetReal1D(pVec3Img, 2));
    
    cvReleaseMat(&pMatCamCoordPos);
    cvReleaseMat(&pMat3DCamPos);
}

CvPoint CMonocularCam::CvtWorld2Img(double x, double y, double z)
{
    CvMat* pMatImgCoord = cvCreateMat(3, 1,CV_64FC1);
    CvMat* pMatWorldCoord = cvCreateMat(4, 1,CV_64FC1);
    
    cvSetReal1D(pMatWorldCoord, 0, x);
    cvSetReal1D(pMatWorldCoord, 1, y);
    cvSetReal1D(pMatWorldCoord, 2, z);
    cvSetReal1D(pMatWorldCoord, 3, 1);
    CvtWorld2Img(pMatImgCoord, pMatWorldCoord);
    
    double dScale = 1.0f / cvGetReal1D(pMatImgCoord, 2);
    CvPoint imgPos;
    imgPos.x = cvRound(cvGetReal1D(pMatImgCoord, 0) * dScale);
    imgPos.y = cvRound(cvGetReal1D(pMatImgCoord, 1) * dScale);
    
    cvReleaseMat(&pMatImgCoord);
    cvReleaseMat(&pMatWorldCoord);
    
    return imgPos;
}

void CMonocularCam::CvtCam2World(CvMat* pVec3World, CvMat* pVec3Cam)
{
    CvMat* pMatTranslation = cvCreateMat(3, 1, CV_64FC1);
    cvSetReal1D(pMatTranslation, 0, cvGetReal2D(m_pMatExtr, 0, 3));
    cvSetReal1D(pMatTranslation, 1, cvGetReal2D(m_pMatExtr, 1, 3));
    cvSetReal1D(pMatTranslation, 2, cvGetReal2D(m_pMatExtr, 2, 3));
    
    CvMat* pMatRotation = cvCreateMat(3, 3, CV_64FC1);
    for(int y = 0; y < 3; ++y)
    {
        for(int x = 0; x < 3; ++x)
        {
            cvSetReal2D(pMatRotation, y, x, cvGetReal2D(m_pMatExtr, y, x));
        }
    }
    
    CvMat* pMatInverseRotation = cvCreateMat(3, 3, CV_64FC1);
    cvTranspose(pMatRotation, pMatInverseRotation);
    
    CvMat* pMatTemp = cvCreateMat(3, 1, CV_64FC1);
    cvSub(pVec3Cam, pMatTranslation, pMatTemp);
    cvMatMul(pMatInverseRotation, pMatTemp, pVec3World);

    cvReleaseMat(&pMatTranslation);
    cvReleaseMat(&pMatRotation);
    cvReleaseMat(&pMatInverseRotation);
    cvReleaseMat(&pMatTemp);
}

CvScalar CMonocularCam::CvtCam2World(double x, double y, double z)
{
    CvMat* pMatCamCoord = cvCreateMat(3, 1, CV_64FC1);
    CvMat* pMatWorldCoord = cvCreateMat(3, 1, CV_64FC1);
    
    cvSetReal1D(pMatCamCoord, 0, x);
    cvSetReal1D(pMatCamCoord, 1, y);
    cvSetReal1D(pMatCamCoord, 2, z);
    
    CvtCam2World(pMatWorldCoord, pMatCamCoord);
    
    CvScalar vecWorldPos;
    vecWorldPos.val[0] = cvGetReal1D(pMatWorldCoord, 0);
    vecWorldPos.val[1] = cvGetReal1D(pMatWorldCoord, 1);
    vecWorldPos.val[2] = cvGetReal1D(pMatWorldCoord, 2);
    
    cvReleaseMat(&pMatCamCoord);
    cvReleaseMat(&pMatWorldCoord);
    
    return vecWorldPos;
}

void CMonocularCam::CvtImg2Cam(CvMat* pVec3Cam, CvMat* pVec3Img, double dDepth)
{
    double rho;
    
    CvMat* pMatInverseIntr = cvCreateMat(3, 3, CV_64FC1);
    cvInvert(m_pMatIntr, pMatInverseIntr);
    
    CvMat* pMatTemp = cvCreateMat(3, 1, CV_64FC1);
    cvMatMul(pMatInverseIntr, pVec3Img, pMatTemp);
    
    rho = dDepth / cvGetReal1D(pMatTemp, 2);
    
    cvSetReal1D(pVec3Cam, 0, cvGetReal1D(pMatTemp, 0) * rho);
    cvSetReal1D(pVec3Cam, 1, cvGetReal1D(pMatTemp, 1) * rho);
    cvSetReal1D(pVec3Cam, 2, cvGetReal1D(pMatTemp, 2) * rho);
    
    cvReleaseMat(&pMatTemp);
    cvReleaseMat(&pMatInverseIntr);
}

// TODO: double check
void CMonocularCam::CvtImg2World(CvMat* pVec3World, CvMat* pVec3Img, double dDepth)
{
    CvMat* pMatInverseIntr = cvCreateMat(3, 3, CV_64FC1);
    cvInvert(m_pMatIntr, pMatInverseIntr);
    
    CvMat* pMatTranslation = cvCreateMat(3, 1, CV_64FC1);
    cvSetReal1D(pMatTranslation, 0, cvGetReal2D(m_pMatExtr, 0, 3));
    cvSetReal1D(pMatTranslation, 1, cvGetReal2D(m_pMatExtr, 1, 3));
    cvSetReal1D(pMatTranslation, 2, cvGetReal2D(m_pMatExtr, 2, 3));
    
    CvMat* pMatRotation = cvCreateMat(3, 3, CV_64FC1);
    for(int y = 0; y < 3; ++y)
    {
        for(int x = 0; x < 3; ++x)
        {
            cvSetReal2D(pMatRotation, x, y, cvGetReal2D(m_pMatExtr, x, y));
        }
    }

    CvMat* pMatDotImg = cvCreateMat(3, 1, CV_64FC1);
    cvMatMul(pMatInverseIntr, pVec3Img, pMatDotImg);
    
    CvMat* pMatTemp = cvCreateMat(3, 3, CV_64FC1);
    cvSetReal2D(pMatTemp, 0, 0, - cvGetReal1D(pMatDotImg, 0));
    cvSetReal2D(pMatTemp, 0, 1, cvGetReal2D(pMatRotation, 0, 0));
    cvSetReal2D(pMatTemp, 0, 2, cvGetReal2D(pMatRotation, 0, 1));
    cvSetReal2D(pMatTemp, 1, 0, - cvGetReal1D(pMatDotImg, 1));
    cvSetReal2D(pMatTemp, 1, 1, cvGetReal2D(pMatRotation, 1, 0));
    cvSetReal2D(pMatTemp, 1, 2, cvGetReal2D(pMatRotation, 1, 1));
    cvSetReal2D(pMatTemp, 2, 0, - cvGetReal1D(pMatDotImg, 2));
    cvSetReal2D(pMatTemp, 2, 1, cvGetReal2D(pMatRotation, 2, 0));
    cvSetReal2D(pMatTemp, 2, 2, cvGetReal2D(pMatRotation, 2, 1));
    
    CvMat* pMatInversTempMatrix = cvCreateMat(3, 3, CV_64FC1);
    cvInvert(pMatTemp, pMatInversTempMatrix);
    
    CvMat* pVecTemp = cvCreateMat(3, 1, CV_64FC1);
    cvSetReal1D(pVecTemp, 0, - (cvGetReal2D(pMatRotation, 0, 2) * dDepth + cvGetReal1D(pMatTranslation, 0)));
    cvSetReal1D(pVecTemp, 1, - (cvGetReal2D(pMatRotation, 1, 2) * dDepth + cvGetReal1D(pMatTranslation, 1)));
    cvSetReal1D(pVecTemp, 2, - (cvGetReal2D(pMatRotation, 2, 2) * dDepth + cvGetReal1D(pMatTranslation, 2)));
    
    CvMat* pMatTempResult = cvCreateMat(3, 1, CV_64FC1);
    cvMatMul(pMatInversTempMatrix, pVecTemp, pMatTempResult);
    
    cvSetReal1D(pVec3World, 0, cvGetReal1D(pMatTempResult, 1));
    cvSetReal1D(pVec3World, 1, cvGetReal1D(pMatTempResult, 2));
    cvSetReal1D(pVec3World, 2, dDepth);
    
    cvReleaseMat(&pMatDotImg);
    cvReleaseMat(&pVecTemp);
    cvReleaseMat(&pMatInversTempMatrix);
    cvReleaseMat(&pMatTemp);
    cvReleaseMat(&pMatTempResult);
    cvReleaseMat(&pMatTranslation);
    cvReleaseMat(&pMatRotation);
    cvReleaseMat(&pMatInverseIntr);
}

CvPoint2D64f CMonocularCam::CvtImg2World(double x, double y, double dDepth)
{
    CvMat* pMatImgCoord = cvCreateMat(3, 1,CV_64FC1);
    CvMat* pMatWorldCoord = cvCreateMat(3, 1,CV_64FC1);
    
    cvSetReal1D(pMatImgCoord, 0, x);
    cvSetReal1D(pMatImgCoord, 1, y);
    cvSetReal1D(pMatImgCoord, 2, 1);
    CvtImg2World(pMatWorldCoord, pMatImgCoord, dDepth);
    
    CvPoint2D64f worldPos;
    worldPos.x = cvGetReal1D(pMatWorldCoord, 0);
    worldPos.y = cvGetReal1D(pMatWorldCoord, 1);
    
    cvReleaseMat(&pMatImgCoord);
    cvReleaseMat(&pMatWorldCoord);
    
    return worldPos;
}

void CMonocularCam::CvtVecCam2World(CvMat* pVec3World, CvMat* pVec3Cam)
{
    CvMat* pMatRotation = cvCreateMat(3, 3, CV_64FC1);
    for(int y = 0; y < 3; ++y)
    {
        for(int x = 0; x < 3; ++x)
        {
            cvSetReal2D(pMatRotation, y, x, cvGetReal2D(m_pMatExtr, y, x));
        }
    }
    
    CvMat* pMatInversRotation = cvCreateMat(3, 3, CV_64FC1);
    cvTranspose(pMatRotation, pMatInversRotation);
    
    cvMatMul(pMatInversRotation, pVec3Cam, pVec3World);
}

CvScalar CMonocularCam::CvtVecCam2World(double x, double y, double z)
{
    CvMat* pMatCamCoord = cvCreateMat(3, 1, CV_64FC1);
    CvMat* pMatWorldCoord = cvCreateMat(3, 1, CV_64FC1);
    
    cvSetReal1D(pMatCamCoord, 0, x);
    cvSetReal1D(pMatCamCoord, 1, y);
    cvSetReal1D(pMatCamCoord, 2, z);
    
    CvtVecCam2World(pMatWorldCoord, pMatCamCoord);
    
    CvScalar vecWorldPos;
    vecWorldPos.val[0] = cvGetReal1D(pMatWorldCoord, 0);
    vecWorldPos.val[1] = cvGetReal1D(pMatWorldCoord, 1);
    vecWorldPos.val[2] = cvGetReal1D(pMatWorldCoord, 2);
    
    cvReleaseMat(&pMatCamCoord);
    cvReleaseMat(&pMatWorldCoord);
    
    return vecWorldPos;
}

void CMonocularCam::DrawInfomation(IplImage* pColorImage, double dSize)
{
    
}