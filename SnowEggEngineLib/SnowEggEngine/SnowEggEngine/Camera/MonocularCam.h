//
//  MonocularCam.h
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#ifndef __MONOCULAR_H__
#define __MONOCULAR_H__

#include "Camera.h"

namespace SnowEgg
{
    namespace Camera
    {
        class CMonocularCam : virtual public CCamera
        {
        public:
            CMonocularCam();
            virtual ~CMonocularCam();
            
        public:
            virtual int GetType() const;
            
        public:
            virtual void operator = (CMonocularCam &right);
            
        public:
            virtual double* GetIntrinsicParameters();
            virtual CvMat* GetIntrinsicMatrix();
            virtual CvMat* GetDistortionCoefficients();
            virtual CvMat* GetExtrinsicMatrix();
            virtual void ConvertExtrinsicParameter(const CvMat* pVecRotation, CvMat* pVecTranslation);
            
        public:
            void SetIntrinsicMatrix(double fx, double fy, double cx, double cy);
            void SetDistortionCoefficients(double k1 = 0.0f, double k2 = 0.0f, double k3 = 0.0f, double k4 = 0.0f);
            void SetExtrinsicMatrix(CvMat& extMat);
            
        public:
            virtual CvScalar GetPosition();
            virtual CvScalar GetLookAt();
            virtual CvScalar GetUpPoint();
            virtual CvScalar GetRightPoint();
            
        public:
            virtual void CvtWorld2Cam(CvMat* pVec4Cam, CvMat* pVec4World);
            virtual void CvtCam2Img(CvMat* pVec3Img, CvMat* pVec3Cam);
            virtual void CvtWorld2Img(CvMat* pVec3Img, CvMat* pVec4World);
            virtual CvPoint CvtWorld2Img(double x, double y, double z);
            virtual void CvtCam2World(CvMat* pVec3World, CvMat* pVec3Cam);
            virtual CvScalar CvtCam2World(double x, double y, double z);
            virtual void CvtImg2Cam(CvMat* pVec3Cam, CvMat* pVec3Img, double dDepth);
            virtual void CvtImg2World(CvMat* pVec3World, CvMat* pVec3Img, double dDepth);
            virtual CvPoint2D64f CvtImg2World(double x, double y, double dDepth = 0.0f);
            virtual void CvtVecCam2World(CvMat* pVec3World, CvMat* pVec3Cam);
            virtual CvScalar CvtVecCam2World(double x, double y, double z);
            
        public:
            virtual void DrawInfomation(IplImage* pColorImage, double dSize);
            
        protected:
            double m_dIntrinsicParams[8]; // Cam coord to img coord
            
            CvMat* m_pMatIntr;
            CvMat* m_pMatExtr;
            CvMat* m_pMatDisCoeffs;
        };
    }
}

#endif /* defined(__MONOCULAR_H__) */
