// SE_UnityBinding.mm

#import <Foundation/Foundation.h>
#import "SnowEggEngine.h"

#pragma mark - Binding

extern "C"
{
    bool SEBinding_Activate()
    {
        NSLog(@"SEBinding_Activate");
        return SnowEgg::CSnowEggEngine::GetInstance()->Activate();
    }
    
    bool SEBinding_Deactivate()
    {
        NSLog(@"SEBinding_Deactivate");
        return SnowEgg::CSnowEggEngine::GetInstance()->Deactivate();
    }
    
    bool SEBinding_StartTracking()
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->StartTracking();
    }
    
    bool SEBinding_StopTracking()
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->StopTracking();
    }
    
    void SEBinding_UpdateRendering()
    {
        SnowEgg::CSnowEggEngine::GetInstance()->RenderCameraFrame();
    }
    
    void* SEBinding_GetOrNewTextureInfo(bool canCreate, int* texW, int* texH, int* camTexW, int* camTexH, int* errorCode)
    {
        NSLog(@"SEBinding_GetOrNewTextureInfo: ");
        
        SnowEgg::CSnowEggEngine* snowEggEngine = SnowEgg::CSnowEggEngine::GetInstance();
        
        int texID   = canCreate ? snowEggEngine->GetOrNewTextureID() : snowEggEngine->GetTextureID();
        *texW       = snowEggEngine->GetTextureWidth();
        *texH       = snowEggEngine->GetTextureHeight();
        *camTexW    = snowEggEngine->GetRenderWidth();
        *camTexH    = snowEggEngine->GetRenderHeight();
        *errorCode  = 0;
        
        return (void*)(intptr_t)texID;
    }
    
    int SEBinding_GetCurrentARStates(SnowEgg::SEBinding_Struct_ARStateNativeMap* pOutArr, int* iElementsNum)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->GetARState(pOutArr, iElementsNum);
    }
    
    int SEBinding_GetCurrentARStatesQuaternion(SnowEgg::SEBinding_Struct_ARStateQuatMap* pOutArr, int* iElementsNum)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->GetARStateQuaternion(pOutArr, iElementsNum);
    }
    
    bool SEBinding_ReleaseBuffer(char* szBuf)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->ReleaseBuffer(szBuf);
    }

    // return null if fail
    int SEBinding_CreateShowcase(int iShowcaseType, int iMaxTracking, int* errorCode)
    {
        char tmpGenID[] = "1";
        
        SnowEgg::CSnowEggEngine* pEngine = SnowEgg::CSnowEggEngine::GetInstance();
        
        char* fndID = pEngine->CreateShowcase(tmpGenID, iMaxTracking, iShowcaseType);
        
        int newID = (fndID == NULL ? -1 : 1);
        
        if (newID == -1)
        {
            *errorCode = 1;
        }
        else
        {
            *errorCode = 0;
        }
        
        // Hard code for testing
        pEngine->CreateMarker("wood352x288Iphone6", 0, "1", "Testing1");
        pEngine->StartTracking();
        
        return newID;
    }
    
    bool SEBinding_ReleaseShowcase(char* szShowcaseID)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->ReleaseShowcase(szShowcaseID);
    }
    
    int SEBinding_GetShowcaseCount()
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->GetShowcaseCount();
    }
    
    bool SEBinding_EnableShowcase(char* szShowcase)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->EnableShowcase(szShowcase);
    }
    
    bool SEBinding_DisableShowcase(char* szShowcase)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->DisableShowcase(szShowcase);
    }
    
    bool SEBinding_CreateMarker(char* szMarkerSource, int iMarkerType, char* szShowcaseID, char* szSpecifiedID, bool bAutoLoad)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->CreateMarker(szMarkerSource, iMarkerType, szShowcaseID, szSpecifiedID, bAutoLoad);
    }
    
    bool SEBinding_ReleaseMarker(char* szMarkerID, char* szShowcaseID)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->ReleaseMarker(szMarkerID, szShowcaseID);
    }
    
    bool SEBinding_LoadMarker(char* szMarkerID, char* szShowcaseID)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->LoadMarker(szMarkerID, szShowcaseID);
    }
    
    bool SEBinding_UnloadMarker(char* szMarkerID, char* szShowcaseID)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->UnloadMarker(szMarkerID, szShowcaseID);
    }
    
    int SEBinding_GetMarkerCount(char* szShowcaseID)
    {
        return SnowEgg::CSnowEggEngine::GetInstance()->GetMarkerCount(szShowcaseID);
    }
}
