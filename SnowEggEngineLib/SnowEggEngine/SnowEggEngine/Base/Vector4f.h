//
//  Vector4f.h
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#ifndef __VECTOR4F_H__
#define __VECTOR4F_H__

#include <math.h>

namespace SnowEgg
{
    struct CVector4f
    {
    public:
        CVector4f():x(0.0f), y(0.0f), z(0.0f), w(1.0f){}
        CVector4f(double x, double y, double z);
        CVector4f(double x, double y, double z, double w);
        
    public:
        CVector4f operator + (const CVector4f &rhs) const;
        void operator += (const CVector4f &rhs);
        CVector4f operator - (const CVector4f &rhs) const;
        void operator -= (const CVector4f &rhs);
        double operator * (const CVector4f &rhs) const; // Dot
        CVector4f operator ^ (const CVector4f &rhs) const; // Cross
        void operator = (const CVector4f rhs);
        CVector4f operator - () const;
        CVector4f operator * (const double s) const;
        void operator *= (const double s);
        CVector4f operator / (const double s) const;
        void operator /= (const double s);
        bool operator == (const CVector4f &rhs) const;
        
    public:
        double GetLength() const;
        double GetDistance(const CVector4f &rhs) const;
        double SetScaleValue(); // set scale value to w
        void SetZero();
        
    public:
        union
        {
            struct
            {
                double x;
                double y;
                double z;
                double w;
            };
            struct
            {
                double v[4];
            };
        };
    };
    
    inline CVector4f CVector4f::operator + (const CVector4f &rhs) const
    {
        return CVector4f(this->x + rhs.x, this->y + rhs.y, this->z + rhs.z, this->w + rhs.w);
    }
    
    inline void CVector4f::operator += (const CVector4f &rhs)
    {
        this->x += rhs.x;
        this->y += rhs.y;
        this->z += rhs.z;
        this->w += rhs.w;
    }
    
    inline CVector4f CVector4f::operator - (const CVector4f &rhs) const
    {
        return CVector4f(this->x - rhs.x, this->y - rhs.y, this->z - rhs.z, this->w - rhs.w);
    }
    
    inline void CVector4f::operator -= (const CVector4f &rhs)
    {
        this->x -= rhs.x;
        this->y -= rhs.y;
        this->z -= rhs.z;
        this->w -= rhs.w;
    }
    
    inline double CVector4f::operator * (const CVector4f &rhs) const
    {
        return this->x * rhs.x + this->y * rhs.y + this->z * rhs.z;
    }
    
    inline CVector4f CVector4f::operator ^ (const CVector4f &rhs) const
    {
        return CVector4f(this->y*rhs.z - this->z*rhs.y,
                         this->z*rhs.x - this->x*rhs.z,
                         this->x*rhs.y - this->y*rhs.x, 1);
    }
    
    inline void CVector4f::operator = (const CVector4f rhs)
    {
        this->x = rhs.x;
        this->y = rhs.y;
        this->z = rhs.z;
        this->w = rhs.w;
    }
    
    inline CVector4f CVector4f::operator - () const
    {
        return CVector4f(-this->x, -this->y, -this->z, -this->w);
    }
    
    inline CVector4f CVector4f::operator * (const double s) const
    {
        return CVector4f(this->x * s, this->y * s, this->z * s, this->w * s);
    }
    
    inline void CVector4f::operator *= (const double s)
    {
        this->x *= s;
        this->y *= s;
        this->z *= s;
        this->w *= s;
    }
    
    inline CVector4f CVector4f::operator / (const double s) const
    {
        return CVector4f(this->x / s, this->y / s, this->z / s, this->w / s);
    }
    
    inline void CVector4f::operator /= (const double s)
    {
        this->x /= s;
        this->y /= s;
        this->z /= s;
        this->w /= s;
    }
    
    inline bool CVector4f::operator == (const CVector4f &rhs) const
    {
        if(this->x == rhs.x && this->y == rhs.y && this->z == rhs.z)
            return true;
        else
            return false;
    }
    
    inline double CVector4f::GetLength() const
    {
        return sqrt(pow(this->x , 2) + pow(this->y, 2) + pow(this->z, 2) + pow(this->w, 2));
    }
    
    inline double CVector4f::GetDistance(const CVector4f &rhs) const
    {
        return sqrt(pow(this->x - rhs.x, 2) + pow(this->y - rhs.y, 2) + pow(this->z - rhs.z, 2) + pow(this->w - rhs.w, 2));
    }
    
    inline double CVector4f::SetScaleValue()
    {
        return this->w = sqrt(pow(this->x , 2) + pow(this->y, 2) + pow(this->z, 2));
    }
    
    inline void CVector4f::SetZero()
    {
        this->x = 0.0f;
        this->y = 0.0f;
        this->z = 0.0f;
        this->w = 0.0f;
    }
}

#endif /* defined(__VECTOR4F_H__) */
