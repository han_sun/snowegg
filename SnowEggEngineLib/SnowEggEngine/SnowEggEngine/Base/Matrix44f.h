//
//  Matrix44f.h
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#ifndef __MATRIX44F_H__
#define __MATRIX44F_H__

#include "memory.h"
#include "Vector4f.h"

namespace SnowEgg
{
    struct CMatrix44f
    {
    public:
        CMatrix44f();
        CMatrix44f(double m11, double m12, double m13, double m14,
                   double m21, double m22, double m23, double m24,
                   double m31, double m32, double m33, double m34,
                   double m41, double m42, double m43, double m44);
        
    public:
        CMatrix44f operator + (const CMatrix44f &rhs) const;
        CMatrix44f operator - (const CMatrix44f &rhs) const;
        CMatrix44f operator * (const CMatrix44f &rhs) const;
        CMatrix44f operator - () const;
        CVector4f operator * (const CVector4f &rhs) const;
        CMatrix44f& operator = (const CMatrix44f &rhs);
        
    public:
        CMatrix44f Transpose() const;
        CMatrix44f Inverse() const;
        
    public:
        union
        {
            struct
            {
                double m[4][4];
            };
            struct
            {
                double m1[16];
            };
            struct
            {
                double	_11, _12, _13, _14,
                        _21, _22, _23, _24,
                        _31, _32, _33, _34,
                        _41, _42, _43, _44;
            };
        };
    };
}

#endif /* defined(__MATRIX44F_H__) */
