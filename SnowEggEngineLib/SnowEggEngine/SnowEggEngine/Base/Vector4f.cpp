//
//  Vector4f.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 6/03/2015.
//
//

#include "Vector4f.h"

using namespace SnowEgg;

CVector4f::CVector4f(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = 1;
}

CVector4f::CVector4f(double x, double y, double z, double w)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
}

