//
//  Defines.h
//  SnowEggEngine
//
//  Created by Han Sun on 24/02/2015.
//
//

#ifndef __DEFINES_H__
#define __DEFINES_H__

// TODO: Make this to be a config file

// Camera Frame ******************
#define WIDTH_CAM           352
#define HEIGHT_CAM          288

// Feature ***********************
#define THRES_DETECT        50.0f
#define THRES_ROUTINE       100.0f
#define MAX_ROUTINE         10

// Matcher ***********************
#define RATIO_KNN           0.7f
#define TYPE_DESC_DATA      CV_32F
#define NUM_MAX_ITER        20
#define DEF_ATTENUATION     1
#define INIT_QUALITY        350

// Tracker ***********************
#define NUM_MAX_TRACK       2000
#define NUM_MAX_TRACKITER   20
#define EPS_TRACK           0.3f
#define SIZE_TRACK_WINDOW   8
#define NUM_LV_PYR          8
#define WIDTH_TRACK_IMG     352
#define HEIGHT_TRACK_IMG    288

// Estimator **********************
#define NUM_MAX_EST_ITER    1000
#define NUM_CONFIDENCE      0.995f
#define REPROERR_PROSAC     10.0f
#define REPROERR_OUTLIER    10.0f
#define NUM_SAMPLING        4
#define NUM_MAX_REFITER     10
#define NUM_MIN_PROSAC      10

// Camera *************************
#define TYPE_CAM_MONO       0
#define TYPE_CAM_BINO       1

// Repo ***************************
#define TYPE_TRACKABLE_PL   0

// Showcase ***********************
#define TIME_STEP           20
#define MIN_MATCH           20
#define MAX_MATCH           60
#define MIN_SHOW            15
#define TYPE_SC_PLANAR      0
#define TYPE_SC_PC          1
#define WIDTH_DEC           352
#define HEIGHT_DEC          288

// For all ************************
#define MAX_LENGTH_ID       20

// UNITY Binding API **************
#define API_ERR_NONE        0
#define API_ERR_GENERAL     1

// Others *************************
#define SAFE_DELETE(x) { if (NULL != x) { delete x; x = NULL;}}
#define SAFE_DELETE_ARR(x) { if (NULL != x) { delete[] x; x = NULL;}}

#endif
