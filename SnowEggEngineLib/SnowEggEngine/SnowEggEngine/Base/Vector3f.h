//
//  Vector3d.h
//  SnowEggEngine
//
//  Created by Han Sun on 4/03/2015.
//
//

#ifndef __VECTOR3F_H__
#define __VECTOR3F_H__

#include <math.h>

namespace  SnowEgg
{
    struct CVector3f
    {
    public:
        CVector3f():x(0.0f), y(0.0f), z(0.0f){};
        CVector3f(double x, double y, double z);
        CVector3f(double x, double y);
        
    public:
        CVector3f operator + (const CVector3f& right) const;
        void operator += (const CVector3f& right);
        CVector3f operator - (const CVector3f& right) const;
        void operator -= (const CVector3f& right);
        double operator * (const CVector3f& right) const;
        void operator = (const CVector3f& right);
        CVector3f operator - () const;
        CVector3f operator * (const double dMulti) const;
        void operator *= (const double dMulti);
        CVector3f operator / (const double dDiv) const;
        void operator /= (const double dDiv);
        
    public:
        double GetLength() const;
        double GetDistance(const CVector3f &right) const;
        
    public:
        void SetZero();
        
    public:
        union
        {
            struct
            {
                double x;
                double y;
                double z;
            };
            struct
            {
                double v[3];
            };
        };
    };
    
    inline CVector3f CVector3f::operator + (const CVector3f& right) const
    {
        return CVector3f(this->x + right.x, this->y + right.y, this->z + right.z);
    }
    
    inline void CVector3f::operator += (const CVector3f& right)
    {
        this->x += right.x;
        this->y += right.y;
        this->z += right.z;
    }
    
    inline CVector3f CVector3f::operator - (const CVector3f& right) const
    {
        return CVector3f(this->x - right.x, this->y - right.y, this->z - right.z);
    }
    
    inline void CVector3f::operator -= (const CVector3f& right)
    {
        this->x -= right.x;
        this->y -= right.y;
        this->z -= right.z;
    }
    
    inline double CVector3f::operator * (const CVector3f& right) const
    {
        return this->x * right.x + this->y * right.y + this->z * right.z;
    }
    
    inline void CVector3f::operator = (const CVector3f& right)
    {
        this->x = right.x;
        this->y = right.y;
        this->z = right.z;
    }
    
    inline CVector3f CVector3f::operator - () const
    {
        return CVector3f(-this->x, -this->y, -this->z);
    }
    
    inline CVector3f CVector3f::operator * (const double dMulti) const
    {
        return CVector3f(this->x * dMulti, this->y * dMulti, this->z * dMulti);
    }
    
    inline void CVector3f::operator *= (const double dMulti)
    {
        this->x *= dMulti;
        this->y *= dMulti;
        this->z *= dMulti;
    }
    
    inline CVector3f CVector3f::operator / (const double dDiv) const
    {
        return CVector3f(this->x / dDiv, this->y / dDiv, this->z / dDiv);
    }
    
    inline void CVector3f::operator /= (const double dDiv)
    {
        this->x /= dDiv;
        this->y /= dDiv;
        this->z /= dDiv;
    }
    
    inline double CVector3f::GetLength() const
    {
        return sqrt(pow(this->x , 2) + pow(this->y, 2) + pow(this->z, 2));
    }
    
    inline void CVector3f::SetZero()
    {
        this->x = 0.0f;
        this->y = 0.0f;
        this->z = 0.0f;
    }
    
    inline double CVector3f::GetDistance(const CVector3f &right) const
    {
        return sqrt(pow(this->x - right.x, 2) + pow(this->y - right.y, 2) + pow(this->z - right.z, 2));
    }
}

#endif /* defined(__VECTOR3F_H__) */
