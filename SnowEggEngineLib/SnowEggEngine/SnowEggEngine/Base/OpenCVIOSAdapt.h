//
//  OpenCVIOSAdapt.h
//  SnowEggEngine
//
//  Created by Han Sun on 24/02/2015.
//
//

#ifndef __OPENCVIOSADAPT_H__
#define __OPENCVIOSADAPT_H__

#import <opencv2/opencv.hpp>
#import <opencv2/legacy/legacy.hpp>
#import <opencv2/highgui/cap_ios.h>
#import <opencv2/legacy/compat.hpp>
#import <opencv2/core/types_c.h>

#endif
