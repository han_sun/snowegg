//
//  Matrix33f.h
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#ifndef __MATRIX33F_H__
#define __MATRIX33F_H__

#import "Vector3f.h"

namespace SnowEgg
{
    struct CMatrix33f
    {
    public:
        CMatrix33f();
        CMatrix33f(double m11, double m12, double m13,
                   double m21, double m22, double m23,
                   double m31, double m32, double m33);
        
    public:
        CMatrix33f operator + (const CMatrix33f &right) const;
        CMatrix33f operator - (const CMatrix33f &right) const;
        CMatrix33f operator * (const CMatrix33f &right) const;
        CVector3f operator * (const CVector3f &right) const;
        CMatrix33f& operator = (const CMatrix33f &right);
        
    public:
        CMatrix33f Transpose() const;
        CMatrix33f Inverse() const;
        
    public:
        union
        {
            struct
            {
                double m[3][3];
            };
            struct
            {
                double m1[9];
            };
            struct
            {
                double	_11, _12, _13,
                        _21, _22, _23,
                        _31, _32, _33;
            };
        };
    };
    
    inline CMatrix33f CMatrix33f::operator + (const CMatrix33f &right) const
    {
        return	CMatrix33f(m[0][0] + right.m[0][0],	m[0][1] + right.m[0][1],	m[0][2] + right.m[0][2],
                           m[1][0] + right.m[1][0],	m[1][1] + right.m[1][1],	m[1][2] + right.m[1][2],
                           m[2][0] + right.m[2][0],	m[2][1] + right.m[2][1],	m[2][2] + right.m[2][2]);
    }
    
    inline CMatrix33f CMatrix33f::operator - (const CMatrix33f &right) const
    {
        return	CMatrix33f(m[0][0] - right.m[0][0],	m[0][1] - right.m[0][1],	m[0][2] - right.m[0][2],
                           m[1][0] - right.m[1][0],	m[1][1] - right.m[1][1],	m[1][2] - right.m[1][2],
                           m[2][0] - right.m[2][0],	m[2][1] - right.m[2][1],	m[2][2] - right.m[2][2]);
    }
    
    inline CMatrix33f CMatrix33f::operator * (const CMatrix33f &right) const
    {
        return CMatrix33f(m[0][0] * right.m[0][0] + m[0][1] * right.m[1][0] + m[0][2] * right.m[2][0],
                          m[0][0] * right.m[0][1] + m[0][1] * right.m[1][1] + m[0][2] * right.m[2][1],
                          m[0][0] * right.m[0][2] + m[0][1] * right.m[1][2] + m[0][2] * right.m[2][2],
                          m[1][0] * right.m[0][0] + m[1][1] * right.m[1][0] + m[1][2] * right.m[2][0],
                          m[1][0] * right.m[0][1] + m[1][1] * right.m[1][1] + m[1][2] * right.m[2][1],
                          m[1][0] * right.m[0][2] + m[1][1] * right.m[1][2] + m[1][2] * right.m[2][2],
                          m[2][0] * right.m[0][0] + m[2][1] * right.m[1][0] + m[2][2] * right.m[2][0],
                          m[2][0] * right.m[0][1] + m[2][1] * right.m[1][1] + m[2][2] * right.m[2][1],
                          m[2][0] * right.m[0][2] + m[2][1] * right.m[1][2] + m[2][2] * right.m[2][2]);
    }
    
    inline CMatrix33f CMatrix33f::Transpose() const
    {
        return CMatrix33f(m[0][0], m[1][0], m[2][0],
                          m[0][1], m[1][1], m[2][1],
                          m[0][2], m[1][2], m[2][2]);
    }
    
    inline CMatrix33f CMatrix33f::Inverse() const
    {
        long double temp = (this->m[0][0] * (this->m[1][1] * this->m[2][2] - this->m[1][2] * this->m[2][1]) -
                            this->m[0][1] * (this->m[1][0] * this->m[2][2] - this->m[1][2] * this->m[2][0]) +
                            this->m[0][2] * (this->m[1][0] * this->m[2][1] - this->m[1][1] * this->m[2][1]));
        
        long double detM = 1.0f / temp;
        return CMatrix33f((this->m[1][1] * this->m[2][2] - this->m[1][2] * this->m[2][1]) * detM,
                          (this->m[0][2] * this->m[2][1] - this->m[0][1] * this->m[2][2]) * detM,
                          (this->m[0][1] * this->m[1][2] - this->m[0][2] * this->m[1][1]) * detM,
                          (this->m[1][2] * this->m[2][0] - this->m[1][0] * this->m[2][2]) * detM,
                          (this->m[0][0] * this->m[2][2] - this->m[0][2] * this->m[2][0]) * detM,
                          (this->m[0][2] * this->m[1][0] - this->m[0][0] * this->m[1][2]) * detM,
                          (this->m[1][0] * this->m[2][1] - this->m[1][1] * this->m[2][0]) * detM,
                          (this->m[0][1] * this->m[2][0] - this->m[0][0] * this->m[2][1]) * detM,
                          (this->m[0][0] * this->m[1][1] - this->m[0][1] * this->m[1][0]) * detM);
    }
    
    inline CVector3f CMatrix33f::operator * (const CVector3f &right) const
    {
        return CVector3f(this->m[0][0] * right.x + this->m[0][1] * right.y + this->m[0][2] * right.z,
                         this->m[1][0] * right.x + this->m[1][1] * right.y + this->m[1][2] * right.z,
                         this->m[2][0] * right.x + this->m[2][1] * right.y + this->m[2][2] * right.z);
    }
    
    inline CMatrix33f& CMatrix33f::operator = (const CMatrix33f &right)
    {
        this->m[0][0] = right.m[0][0];	this->m[0][1] = right.m[0][1];	this->m[0][2] = right.m[0][2];
        this->m[1][0] = right.m[1][0];	this->m[1][1] = right.m[1][1];	this->m[1][2] = right.m[1][2];
        this->m[2][0] = right.m[2][0];	this->m[2][1] = right.m[2][1];	this->m[2][2] = right.m[2][2];
        
        return *this;
    }
}

#endif /* defined(__MATRIX33F_H__) */
