//
//  Vector3d.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 4/03/2015.
//
//

#include "Vector3f.h"

using namespace SnowEgg;

CVector3f::CVector3f(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

CVector3f::CVector3f(double x, double y)
{
    this->x = x;
    this->y = y;
    this->z = 0.0f;
}
