//
//  Geometry.h
//  SnowEggEngine
//
//  Created by Han Sun on 2/03/2015.
//
//

#ifndef __POINT2I_H__
#define __POINT2I_H__

namespace SnowEgg
{
    struct CPoint2I
    {
    public:
        CPoint2I():x(0), y(0){};
        
    public:
        union
        {
            struct
            {
                int x;
                int y;
            };
            struct
            {
                int v[2];
            };
        };
    };
}

#endif
