//
//  Matrix33f.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#include "Matrix33f.h"

using namespace SnowEgg;

CMatrix33f::CMatrix33f()
{
    m[0][0] = 0;	m[0][1] = 0;	m[0][2] = 0;
    m[1][0] = 0;	m[1][1] = 0;	m[1][2] = 0;
    m[2][0] = 0;	m[2][1] = 0;	m[2][2] = 0;
}

CMatrix33f::CMatrix33f(double m11, double m12, double m13,
                       double m21, double m22, double m23,
                       double m31, double m32, double m33)
{
    m[0][0] = m11;	m[0][1] = m12;	m[0][2] = m13;
    m[1][0] = m21;	m[1][1] = m22;	m[1][2] = m23;
    m[2][0] = m31;	m[2][1] = m32;	m[2][2] = m33;
}