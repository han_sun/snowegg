//
//  PlanarShowcase.h
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#ifndef __PLANARSHOWCASE_H__
#define __PLANARSHOWCASE_H__

#include <stdio.h>
#include <vector>
#include "Showcase.h"
#include "Decorator.h"
#include "../Estimator/PlanarEstimator.h"
#include "../Tracker/PyrLKTracker.h"
#include "../Camera/MonocularCam.h"
#include "../Feature/FeaturePoint.h"
#include "../Feature/FeaturePointDetector.h"

namespace SnowEgg
{
    namespace Showcase
    {
        class CPlanarShowcase : virtual public CShowcase
        {
        public:
            CPlanarShowcase();
            virtual ~CPlanarShowcase();
            
        public:
            virtual bool CreateTrackable(char* szSource, char* szSpecified);
            virtual bool ReleaseTrackable(char* szID);
            virtual bool LoadTrackable(char* szID);
            virtual bool UnloadTrackable(char* szID);
            
        public:
            virtual bool UpdateCamPose(cv::Mat& image);
            
        public:
            virtual int GetType() const;
            
        public:
            virtual void Enable();
            virtual void Disable();
            
        public:
            void SetGravity(double x, double y, double z);
            
        protected:
            virtual void ClearSceFPList();
            
        protected:
            virtual bool UpdateTrackable(int iIndex);
            
        protected:
            Tracker::CPyrLKTracker* m_pTracker;
            Estimator::CPlanarEstimator* m_pEstimator;
            Feature::CFeaturePointDetector* m_pDetector;
            CDecorator* m_pDecorator;
            
        protected:
            IplImage* m_pCurGrayImg;
            IplImage* m_pPreGrayImg;
            IplImage* m_pCurColorImg;
            std::vector<Feature::CFeaturePoint*>* m_pVecSceFP;
            
        protected:
            bool m_bEnable;
        };
    }
}

#endif /* defined(__PLANARSHOWCASE_H__) */
