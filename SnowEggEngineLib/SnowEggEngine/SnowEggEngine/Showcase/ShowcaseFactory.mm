//
//  ShowcaseFactory.cpp
//  SnowEgg
//
//  Created by Han Sun on 9/04/2015.
//  Copyright (c) 2015 Auggd. All rights reserved.
//

#include "ShowcaseFactory.h"

using namespace SnowEgg;
using namespace Showcase;

CShowcaseFactory::CShowcaseFactory()
{

}

CShowcaseFactory::~CShowcaseFactory()
{

}

CShowcase* CShowcaseFactory::Create(int iType)
{
    CShowcase* pNewShowcase = NULL;
    
    switch (iType)
    {
        case TYPE_SC_PLANAR:
            pNewShowcase = new CPlanarShowcase();
            break;
            
        case TYPE_SC_PC:
            break;
            
        default:
            break;
    }
    
    return pNewShowcase;
}