//
//  Decoration.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 16/03/2015.
//
//

#include "Decorator.h"

using namespace SnowEgg;
using namespace Showcase;

CDecorator::CDecorator()
{
    cvInitFont(&m_font, CV_FONT_HERSHEY_SIMPLEX,
               1.0, 1.0, 0.0,
               1, 8);
    
    m_bFPS = true;
    m_bPoints = true;
    
    m_fGavityX = 0.0f;
    m_fGavityY = 0.0f;
    m_fGavityZ = 0.0f;
}

CDecorator::~CDecorator()
{
    
}

double dLastTime = 0.0f;
double dCurTime = 0.0f;

void CDecorator::DrawRoutine(IplImage* frame)
{
    dCurTime = CACurrentMediaTime();
    CFTimeInterval elapsedTime = dCurTime - dLastTime;
    dLastTime = dCurTime;
    elapsedTime = 1.0f / elapsedTime;
    
    std::ostringstream strs;
    strs << "FPS:" << static_cast<int>(elapsedTime);
    std::string strFPS = strs.str();

    if (m_bFPS)
    {
        cvPutText(frame, strFPS.c_str(), cvPoint(10, 30), &m_font, cvScalar(255, 255, 255));
    }
}

void CDecorator::Makeup(IplImage* frame, Repository::CTrackable* pTrackable, int iIndex)
{
    if (NULL == frame || NULL == pTrackable)
    {
        return;
    }
    
    Repository::CPlanarTrackable* pPlanarTrackable = dynamic_cast<Repository::CPlanarTrackable*>(pTrackable);
    
    
    if (m_bPoints)
    {
        int iMatchedCount = static_cast<int>(pPlanarTrackable->GetMatchedList()->size());
        // Draw matched points
        for (int i = 0; i < iMatchedCount; ++i)
        {
            double x = (*pPlanarTrackable->GetMatchedList())[i]->GetSceFeaturePoint()->GetPosition().x;
            double y = (*pPlanarTrackable->GetMatchedList())[i]->GetSceFeaturePoint()->GetPosition().y;
            CvPoint referencePoint = cvPoint(cvRound(x), cvRound(y));
            cvCircle(frame, referencePoint, 1, CV_RGB(0, 255, 255), CV_FILLED);
        }
        
        std::ostringstream strs;
        strs << "Matched:" << iMatchedCount;
        std::string strMatched = strs.str();
        
        if (m_bFPS)
        {
            cvPutText(frame, strMatched.c_str(), cvPoint(10, 60), &m_font, cvScalar(255, 255, 255));
        }
    }
    
    if (pPlanarTrackable->GetMatchedList()->size() >= MIN_SHOW)
    {
        Camera::CMonocularCam* pCam = pPlanarTrackable->GetCamera();
        CvScalar color = CV_RGB(0, 255, 0);
        CvScalar color2 = CV_RGB(255, 0, 0);
        switch (iIndex)
        {
            case 2:
            {
                CvScalar worldPoint = pCam->CvtVecCam2World(m_fGavityY, m_fGavityX, -m_fGavityZ);
                //CvScalar worldPoint = pCam->CvtVecCam2World(m_fGavityZ, m_fGavityY, m_fGavityX);
                
                float fWidth = 20.0f;
                
                static float fOffsetX = 0.0f;
                static float fOffsetY = 0.0f;
                
                fOffsetX += worldPoint.val[0] * 7.0f;
                fOffsetY += worldPoint.val[1] * 7.0f;
                
                cvLine(frame, pCam->CvtWorld2Img(-fWidth + fOffsetX, -fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(+fWidth + fOffsetX, -fWidth + fOffsetY, 0.0f),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(+fWidth + fOffsetX, -fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(+fWidth + fOffsetX, +fWidth + fOffsetY, 0.0f),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(+fWidth + fOffsetX, +fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(-fWidth + fOffsetX, +fWidth + fOffsetY, 0.0f),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(-fWidth + fOffsetX, +fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(-fWidth + fOffsetX, -fWidth + fOffsetY, 0.0f),	color2, 1);
                
                cvLine(frame, pCam->CvtWorld2Img(-fWidth + fOffsetX, -fWidth + fOffsetY, fWidth),	pCam->CvtWorld2Img(+fWidth + fOffsetX, -fWidth + fOffsetY, fWidth),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(+fWidth + fOffsetX, -fWidth + fOffsetY, fWidth),	pCam->CvtWorld2Img(+fWidth + fOffsetX, +fWidth + fOffsetY, fWidth),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(+fWidth + fOffsetX, +fWidth + fOffsetY, fWidth),	pCam->CvtWorld2Img(-fWidth + fOffsetX, +fWidth + fOffsetY, fWidth),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(-fWidth + fOffsetX, +fWidth + fOffsetY, fWidth),	pCam->CvtWorld2Img(-fWidth + fOffsetX, -fWidth + fOffsetY, fWidth),	color2, 1);
                
                cvLine(frame, pCam->CvtWorld2Img(-fWidth + fOffsetX, -fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(-fWidth + fOffsetX, -fWidth + fOffsetY, fWidth),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(+fWidth + fOffsetX, -fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(+fWidth + fOffsetX, -fWidth + fOffsetY, fWidth),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(+fWidth + fOffsetX, +fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(+fWidth + fOffsetX, +fWidth + fOffsetY, fWidth),	color2, 1);
                cvLine(frame, pCam->CvtWorld2Img(-fWidth + fOffsetX, +fWidth + fOffsetY, 0.0f),	pCam->CvtWorld2Img(-fWidth + fOffsetX, +fWidth + fOffsetY, fWidth),	color2, 1);
                
                break;
            }
                
            case 1:
                cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/2, -HEIGHT_DEC/2, 0.0),	pCam->CvtWorld2Img(+WIDTH_DEC/2, -HEIGHT_DEC/2, 0.0),	color, 1);
                cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/2, -HEIGHT_DEC/2, 0.0),	pCam->CvtWorld2Img(+WIDTH_DEC/2, +HEIGHT_DEC/2, 0.0),	color, 1);
                cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/2, +HEIGHT_DEC/2, 0.0),	pCam->CvtWorld2Img(-WIDTH_DEC/2, +HEIGHT_DEC/2, 0.0),	color, 1);
                cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/2, +HEIGHT_DEC/2, 0.0),	pCam->CvtWorld2Img(-WIDTH_DEC/2, -HEIGHT_DEC/2, 0.0),	color, 1);
                
                break;
            case 0:
                for (int i = 2; i < 10; ++i)
                {
                    cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(+WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(+WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(-WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(-WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                }
                break;
            case 3:
                for (int i = 2; i < 4; ++i)
                {
                    cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(+WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(+WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(-WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(-WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                }
                
                break;
            case 4:
                for (int i = 4; i < 15; ++i)
                {
                    cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(+WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(+WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(+WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(-WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                    cvLine(frame, pCam->CvtWorld2Img(-WIDTH_DEC/i, +HEIGHT_DEC/i, (i-2)*50.0), pCam->CvtWorld2Img(-WIDTH_DEC/i, -HEIGHT_DEC/i, (i-2)*50.0), color, 1);
                }
                break;
                
            default:
                break;
        }
    }
}

void CDecorator::SetShowFPS(bool bFPS)
{
    m_bFPS = bFPS;
}

bool CDecorator::GetShowFPS() const
{
    return m_bFPS;
}

void CDecorator::SetShowPoint(bool bPoints)
{
    m_bPoints = bPoints;
}

bool CDecorator::GetShowPoint() const
{
    return m_bPoints;
}

void CDecorator::SetGravity(float x, float y, float z)
{
    m_fGavityX = x;
    m_fGavityY = y;
    m_fGavityZ = z;
}