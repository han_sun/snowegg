//
//  Decoration.h
//  SnowEggEngine
//
//  Created by Han Sun on 16/03/2015.
//
//

#ifndef __DECORATION_H__
#define __DECORATION_H__

#include <stdio.h>
#include "../Base/OpenCVIOSAdapt.h"
#include "../Repository/PlanarTrackable.h"

namespace SnowEgg
{
    namespace Showcase
    {
        class CDecorator
        {
        public:
            CDecorator();
            virtual ~CDecorator();
            
        public:
            void Makeup(IplImage* frame, Repository::CTrackable* pTrackable, int iIndex);
            // This should be in a global decorator, will move this in the future
            void DrawRoutine(IplImage* frame);
            
        public:
            void SetShowFPS(bool bFPS);
            bool GetShowFPS() const;
            void SetShowPoint(bool bPoint);
            bool GetShowPoint() const;
            
        public:
            void SetGravity(float x, float y, float z);
            
        protected:
            CvFont m_font;
            bool m_bFPS;
            bool m_bPoints;
            
        protected:
            float m_fGavityX;
            float m_fGavityY;
            float m_fGavityZ;
        };
    }
}


#endif /* defined(__DECORATION_H__) */
