//
//  Showcase.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#include "Showcase.h"

using namespace SnowEgg;
using namespace Showcase;

CShowcase::CShowcase()
{
    m_iMaxTracking = 1;
    memset(m_szState, 0, NUM_MAX_TRACK);
    memset(m_szID, 0, MAX_LENGTH_ID + 1);
}

CShowcase::~CShowcase()
{
    
}

void CShowcase::SetMaxTracking(int iNum)
{
    if (iNum < 0)
    {
        return;
    }
    
    m_iMaxTracking = iNum;
}

int CShowcase::GetMaxTracking() const
{
    return m_iMaxTracking;
}

Repository::CTrackable* CShowcase::GetTrackableByIndex(int iIndex)
{
    int iTrackableCount = static_cast<int>(m_repo.GetTrackableListRef().size());
    
    if (iIndex < 0 || iIndex >= iTrackableCount)
    {
        return NULL;
    }
    
    return m_repo.GetTrackableListRef()[iIndex];
}

int CShowcase::GetTrackableCount()
{
    return static_cast<int>(m_repo.GetTrackableListRef().size());
}

char* CShowcase::GetID()
{
    return m_szID;
}

void CShowcase::SetID(const char* szID)
{
    if (NULL == szID)
    {
        return;
    }
    
    int iLength = static_cast<int>(strlen(szID));
    memset(m_szID, 0, iLength + 1);
    memcpy(m_szID, szID, iLength);
}