//
//  PlanarShowcase.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#include "PlanarShowcase.h"
#include "../Repository/PlanarTrackable.h"
#include "../Feature/SURFExtractor.h"

using namespace SnowEgg;
using namespace Showcase;

CPlanarShowcase::CPlanarShowcase()
{
    memset(m_szID, 0, MAX_LENGTH_ID + 1);
    
    m_pTracker = new Tracker::CPyrLKTracker();
    m_pEstimator = new Estimator::CPlanarEstimator();
    
    m_pDetector = new Feature::CFeaturePointDetector();
    m_pDecorator = new CDecorator();
    
//    m_repo.LoadTrackable("stone352x288Iphone6");
//    m_repo.CreateTrackable("wood352x288Iphone6", "THISISTHEFUCKINGID");
//    m_repo.LoadTrackable("Mag_1_352x288Iphone6");
//    m_repo.LoadTrackable("Mag_36_352x288Iphone6");
    
    m_pVecSceFP = new std::vector<Feature::CFeaturePoint*>();
    
    m_pCurColorImg = cvCreateImage(cvSize(WIDTH_CAM, HEIGHT_CAM), IPL_DEPTH_8U, 3);
    m_pCurGrayImg = cvCreateImage(cvSize(WIDTH_CAM, HEIGHT_CAM), IPL_DEPTH_8U, 1);
    
    m_pPreGrayImg = NULL;
    
    m_bEnable = true;
}

CPlanarShowcase::~CPlanarShowcase()
{
    SAFE_DELETE(m_pEstimator);
    SAFE_DELETE(m_pTracker);
    SAFE_DELETE(m_pDetector);
    SAFE_DELETE(m_pDecorator);
    
    if(NULL != m_pPreGrayImg)
    {
        cvReleaseImage(&m_pPreGrayImg);
        m_pPreGrayImg = NULL;
    }
    
    if(NULL != m_pCurGrayImg)
    {
        cvReleaseImage(&m_pCurGrayImg);
        m_pCurGrayImg = NULL;
    }
    
    if(NULL != m_pCurColorImg)
    {
        cvReleaseImage(&m_pCurColorImg);
        m_pCurColorImg = NULL;
    }
    
    if (NULL != m_pVecSceFP)
    {
        int iSceSize = static_cast<int>(m_pVecSceFP->size());
        for (int i = 0; i < iSceSize; ++i)
        {
            SAFE_DELETE((*m_pVecSceFP)[i]);
        }
        
        m_pVecSceFP->clear();
        SAFE_DELETE(m_pVecSceFP);
    }
}

bool CPlanarShowcase::CreateTrackable(char* szSource, char* szSpecifiedID)
{
    if (m_repo.CreateTrackable(szSource, szSpecifiedID))
    {
        if (NULL != szSpecifiedID)
        {
            memcpy(m_szID, szSpecifiedID, strlen(szSpecifiedID));
        }
        
        return true;
    }
  
    return false;
}

bool CPlanarShowcase::ReleaseTrackable(char* szID)
{
    if (NULL == szID)
    {
        return false;
    }
    
    Repository::CPlanarTrackable* pTrackable = dynamic_cast<Repository::CPlanarTrackable*>(m_repo.GetTrackableByName(szID));
    
    if (NULL == pTrackable)
    {
        return false;
    }
    
    SAFE_DELETE(pTrackable);
    
    return true;
}

bool CPlanarShowcase::LoadTrackable(char* szID)
{
    if (NULL == szID)
    {
        return false;
    }
    
    Repository::CPlanarTrackable* pTrackable = dynamic_cast<Repository::CPlanarTrackable*>(m_repo.GetTrackableByName(szID));
    
    if (NULL == pTrackable)
    {
        return false;
    }

    pTrackable->LoadContent();
    
    return true;
}

bool CPlanarShowcase::UnloadTrackable(char* szID)
{
    if (NULL == szID)
    {
        return false;
    }
    
    Repository::CPlanarTrackable* pTrackable = dynamic_cast<Repository::CPlanarTrackable*>(m_repo.GetTrackableByName(szID));
    
    if (NULL == pTrackable)
    {
        return false;
    }
    
    pTrackable->UnloadContent();
    
    return true;
}

static int s_iTempStep = 0;

bool CPlanarShowcase::UpdateCamPose(cv::Mat& image)
{
    if (!m_bEnable)
    {
        return false;
    }
    
    IplImage colorImage = image;
    m_pCurColorImg = &colorImage;
    
    cvCvtColor(m_pCurColorImg, m_pCurGrayImg, CV_BGR2GRAY);
    
    if (NULL == m_pPreGrayImg)
    {
        m_pPreGrayImg = cvCreateImage(cvSize(WIDTH_CAM, HEIGHT_CAM), IPL_DEPTH_8U, 1);
        cvCopyImage(m_pCurGrayImg, m_pPreGrayImg);
        
        return true;
    }
    
    SnowEgg::Repository::CPlanarTrackable* pPlanarTrackable = NULL;
    int iSize = static_cast<int>(m_repo.GetTrackableListRef().size());
    bool bNeedNews = false;
    
    // Bug here, pPlanarTrackable need to be updated
    for (int i = 0; i < iSize; ++i)
    {
        pPlanarTrackable = dynamic_cast<SnowEgg::Repository::CPlanarTrackable*>(m_repo.GetTrackableListRef()[i]);
        if (pPlanarTrackable->GetMatchedList()->size() < MIN_MATCH)
        {
            bNeedNews = true;
        }
    }
    
    if (++s_iTempStep > TIME_STEP)
    {
        if (bNeedNews)
        {
            s_iTempStep = 0;
            m_pVecSceFP->clear();
            m_pDetector->Detect(m_pCurGrayImg, m_pVecSceFP);
            
            for (int i = 0; i < iSize; ++i)
            {
                pPlanarTrackable = dynamic_cast<SnowEgg::Repository::CPlanarTrackable*>(m_repo.GetTrackableListRef()[i]);
                
                if (pPlanarTrackable->GetMatchedList()->size() < MIN_MATCH)
                {
                    pPlanarTrackable->Match(m_pVecSceFP);
                }
            }
            
            this->ClearSceFPList();
        }
        /*
        else if (pPlanarTrackable->GetMatchedList()->size() < MAX_MATCH)
        {
            m_pVecSceFP->clear();
            m_pDetector->Detect(m_pCurGrayImg, m_pVecSceFP, MAX_ROUTINE, THRES_ROUTINE, pPlanarTrackable->GetMatchedList());
            
            for (int i = 0; i < iSize; ++i)
            {
                pPlanarTrackable = dynamic_cast<SnowEgg::Repository::CPlanarTrackable*>(m_repo.GetTrackableListRef()[i]);
                
                pPlanarTrackable->Match(m_pVecSceFP);
            }
        }*/
    }
    
    for (int i = 0; i < iSize; ++i)
    {
        pPlanarTrackable = dynamic_cast<SnowEgg::Repository::CPlanarTrackable*>(m_repo.GetTrackableListRef()[i]);
        
        bool bNeedShow = this->UpdateTrackable(i);
        
        if (pPlanarTrackable->GetMatchedList()->size() >= MIN_SHOW)
        {
            pPlanarTrackable->SetTracking(true);
            if (bNeedShow)
            {
                m_pDecorator->Makeup(m_pCurColorImg, pPlanarTrackable, i%4);
            }
        }
        else
        {
            pPlanarTrackable->SetTracking(false);
        }
    }

    m_pDecorator->DrawRoutine(m_pCurColorImg);

    cvCopyImage(m_pCurGrayImg, m_pPreGrayImg);
 
    return true;
}

int CPlanarShowcase::GetType() const
{
    return TYPE_SC_PLANAR;
}

void CPlanarShowcase::Enable()
{
    m_bEnable = true;
}

void CPlanarShowcase::Disable()
{
    m_bEnable = false;
}

bool CPlanarShowcase::UpdateTrackable(int iIndex)
{
    SnowEgg::Repository::CPlanarTrackable* pPlanarTrackable = dynamic_cast<SnowEgg::Repository::CPlanarTrackable*>(m_repo.GetTrackableListRef()[iIndex]);
    
    // Track
    m_pTracker->Track(m_pPreGrayImg,
                      m_pCurGrayImg,
                      pPlanarTrackable->GetMatchedList());
    
    // Outlier removal
    // Remove those are not tracked
    int iSize = static_cast<int>(pPlanarTrackable->GetMatchedList()->size());
    Matcher::C2DMatchedPair* pCurMatched = NULL;
    for (int i = 0; i < iSize; ++i)
    {
        pCurMatched = (*pPlanarTrackable->GetMatchedList())[i];
        /*
        if (pCurMatched->Attenuate() < 0)
        {
            pCurMatched->SetIsTracking(false);
        }*/
        
        if (!pCurMatched->IsTracking())
        {
            SAFE_DELETE(pCurMatched);
            pPlanarTrackable->GetMatchedList()->erase(pPlanarTrackable->GetMatchedList()->begin() + i);
            --i;
            --iSize;
        }
    }
    
    // estimate and update cam pose
    bool bShow = m_pEstimator->Calculate(pPlanarTrackable->GetMatchedList(), pPlanarTrackable->GetCamera());
    
    return bShow;
}

void CPlanarShowcase::SetGravity(double x, double y, double z)
{
    m_pDecorator->SetGravity(x, y, z);
}

void CPlanarShowcase::ClearSceFPList()
{
    int iSize = static_cast<int>(m_pVecSceFP->size());
    
    for (int i = 0; i < iSize; ++i)
    {
        SAFE_DELETE((*m_pVecSceFP)[i]);
    }
    
    m_pVecSceFP->clear();
}