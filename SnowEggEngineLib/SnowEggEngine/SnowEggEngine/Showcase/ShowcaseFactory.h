//
//  ShowcaseFactory.h
//  SnowEgg
//
//  Created by Han Sun on 9/04/2015.
//  Copyright (c) 2015 Auggd. All rights reserved.
//

#ifndef __SHOWCASEFACTORY_H__
#define __SHOWCASEFACTORY_H__

#include <stdio.h>
#include "PlanarShowcase.h"
#include "../Base/Defines.h"

namespace SnowEgg
{
    namespace Showcase
    {
        class CShowcaseFactory
        {
        public:
            CShowcaseFactory();
            ~CShowcaseFactory();
            
        public:
            static CShowcase* Create(int iType);
        };
    }
}

#endif /* defined(__SHOWCASEFACTORY_H__) */
