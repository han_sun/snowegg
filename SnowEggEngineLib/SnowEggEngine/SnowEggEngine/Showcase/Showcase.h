//
//  Showcase.h
//  SnowEggEngine
//
//  Created by Han Sun on 10/03/2015.
//
//

#ifndef __SHOWCASE_H__
#define __SHOWCASE_H__

#include <stdio.h>
#include "../Base/OpenCVIOSAdapt.h"
#include "../Repository/Repository.h"
#include "../Base/Defines.h"

namespace SnowEgg
{
    namespace Showcase
    {
        class CShowcase
        {
        public:
            CShowcase();
            virtual ~CShowcase();
            
        public:
            void SetMaxTracking(int iNum);
            int GetMaxTracking() const;
            
        public:
            virtual bool CreateTrackable(char* szSource, char* szSpecified) = 0;
            virtual bool ReleaseTrackable(char* szID) = 0;
            virtual bool LoadTrackable(char* szID) = 0;
            virtual bool UnloadTrackable(char* szID) = 0;
            Repository::CTrackable* GetTrackableByIndex(int iIndex);
            int GetTrackableCount();
            
        public:
            virtual int GetType() const = 0;
            
        public:
            virtual void Enable() = 0;
            virtual void Disable() = 0;
            
        public:
            char* GetID();
            void SetID(const char* szID);
            
        public:
            virtual bool UpdateCamPose(cv::Mat& image) = 0;

        protected:
            int m_iMaxTracking;
            Repository::CRepository m_repo;
            char m_szState[NUM_MAX_TRACK];
            
        protected:
            char m_szID[MAX_LENGTH_ID + 1];
        };
    }
}

#endif /* defined(__SHOWCASE_H__) */
