//
//  TestRenderer.h
//  SnowEgg
//
//  Created by Han Sun on 24/02/2015.
//
//

#ifndef __CAMERADEVICE_H__
#define __CAMERADEVICE_H__

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Base/OpenCVIOSAdapt.h"

@class CameraDevice;

@protocol CameraDeviceDelegate <NSObject>

@optional

- (void)onCameraFrameUpdated:(cv::Mat&)image camDevice:(CameraDevice*)camDevice;

@end

@interface CameraDevice : NSObject
{
    
}

@property (readonly) int camTexWidth;
@property (readonly) int camTexHeight;

- (bool)setDelegate:(id)target;
- (bool)initCam;
- (void)startCam;
- (void)stopCam;
- (void)setTextureID:(int)toId texW:(int)texW texH:(int)texH texFormat:(int)texFormat;
- (void)renderCurrentCameraFrameToTexture;
- (GLuint) getTextureID:(BOOL)createIfNone;
- (int) getTextureWidth;
- (int) getTextureHeight;
- (int) getRenderWidth;
- (int) getRenderHeight;

@end

#endif /* defined(__CAMERADEVICE_H__) */

