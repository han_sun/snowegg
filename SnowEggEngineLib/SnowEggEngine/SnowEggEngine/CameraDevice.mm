//
//  TestRenderer.cpp
//  SnowEgg
//
//  Created by Han Sun on 24/02/2015.
//
//

#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import "Base/OpenCVIOSAdapt.h"
#import "CameraDevice.h"
#import <pthread.h>

using namespace cv;

@interface CameraDevice () <CvVideoCameraDelegate>
{
    Mat             m_currentCameraFrame;   // the data from the last camera frame
    
    Mat             m_frameBuffer0;         // copied into before rendering
    
    pthread_mutex_t m_mutexCameraFrame;     // used to lock the camera frame
    
    GLuint          m_texID;                // destination texture id
    int             m_texW;                 // destination texture width
    int             m_texH;                 // destination texture height
    int             m_pixelFormat;          // (current) destination texture internalFormat
    int             m_destPixelFormat;      // (pending) destination texture internalFormat
    
    id              m_delegateTarget;       // the delegate receiver
    
    int             m_texFlipMode;          // if/how the camera feed texture should flip
}

@property (nonatomic, strong)   CvVideoCamera*          pVideoCamera;

@end

@implementation CameraDevice

#pragma mark - Custom Properties

- (int)camTexWidth
{
    return m_currentCameraFrame.empty() ? -1 : m_currentCameraFrame.cols;
}
- (int)camTexHeight
{
    return m_currentCameraFrame.empty() ? -1 : m_currentCameraFrame.rows;
}

#pragma mark - Interface

-(id) init
{
    if(self = [super init])
    {
        m_mutexCameraFrame = PTHREAD_MUTEX_INITIALIZER;
        m_texFlipMode      = 1;
    }
    
    return self;
}

- (void)startCam
{
    [self.pVideoCamera start];
}

- (void)stopCam
{
    [self.pVideoCamera stop];
}

- (bool)initCam
{
    self.pVideoCamera = [[CvVideoCamera alloc] init];
    
    self.pVideoCamera.delegate = self;
    self.pVideoCamera.defaultAVCaptureDevicePosition    = AVCaptureDevicePositionBack;
    self.pVideoCamera.defaultAVCaptureSessionPreset     = AVCaptureSessionPreset352x288;
    self.pVideoCamera.defaultAVCaptureVideoOrientation  = AVCaptureVideoOrientationLandscapeRight;
    self.pVideoCamera.defaultFPS                        = 30;
    
    return true;
}

- (void)processImage:(cv::Mat&)image
{
    // copy data to our frame buffer
    [self lockCurrentCameraFrame];
    
    // flip if necessary
    {
        if(m_texFlipMode == 1)
        {// flip horizontally
            flip(image, m_currentCameraFrame, 1);
        }
        else if(m_texFlipMode == 2)
        {// flip vertically
            flip(image, m_currentCameraFrame, 0);
        }
        else if(m_texFlipMode == 3)
        {// flip horizontally and vertically
            flip(image, m_currentCameraFrame, -1);
        }
        else
        {
            m_currentCameraFrame = image.clone();
        }
    }
    [self unlockCurrentCameraFrame];
    
    if(m_delegateTarget != nil)
    {
        [m_delegateTarget onCameraFrameUpdated:image camDevice:self];
    }
}

- (bool)setDelegate:(id)target
{
    if(target == nil)
    {
        m_delegateTarget = nil;
        return YES;
    }
    if(![target conformsToProtocol:@protocol(CameraDeviceDelegate)])    { return NO; }
    m_delegateTarget = target;
    
    return YES;
}

- (void)setTextureID:(int)toId texW:(int)texW texH:(int)texH texFormat:(int)texFormat
{
    m_texID = toId;
    m_texW  = texW;
    m_texH  = texH;
    
    int destFormat;
    switch (texFormat) {
        case 1:
            destFormat = GL_RGBA;
            break;
        case 2:
            destFormat = GL_RGB;
            break;
        default:
            destFormat = texFormat;
            break;
    }
    
    m_destPixelFormat = destFormat;
}
- (GLuint) getTextureID:(BOOL)createIfNone
{
    if(createIfNone && m_texID <= 0)
    {
        [self generateNewDestRenderTexture:GL_RGB];
    }
    return m_texID;
}
- (int) getTextureWidth
{
    return m_texW;
}
- (int) getTextureHeight
{
    return m_texH;
}
- (int) getRenderWidth
{
    return self.pVideoCamera.imageWidth;
}
- (int) getRenderHeight
{
    return self.pVideoCamera.imageHeight;
}


- (void)    reencodeMat:(cv::Mat&)mat
             fromFormat:(GLenum)fromFormat
               toFormat:(GLenum)toFormat
{
    if(mat.empty()) { return; }
    int convertCode = [self getCVColorConvertCode:fromFormat toGLCol:toFormat];
    if(convertCode != -1)
    {
        cvtColor(mat, mat, convertCode);
    }
}
- (void)renderCurrentCameraFrameToTexture
{
    if(m_texID == 0)                        { return; }
    
    int colCode = [self getCameraCVColorConvertCode:m_destPixelFormat];
    
    if(colCode == -1)                       { return; }
    
    m_pixelFormat = m_destPixelFormat;
    
    // copy the current camera frame to our buffer
    [self lockCurrentCameraFrame];
    if(!m_currentCameraFrame.empty())
    {
        cvtColor(m_currentCameraFrame, m_frameBuffer0, colCode);
    }
    [self unlockCurrentCameraFrame];
    
    if(m_frameBuffer0.empty())              { return; }
    
    [self applyMatAsSubImage:m_frameBuffer0 intoTextureID:m_texID inputColourFormat:m_pixelFormat];
}

#pragma mark - Logic

#pragma mark CV Helpers
/**
 Find the relevant OpenCV color conversion code for two GL PixelFormats
 
 Note that only GL_RGBA and GL_RGB are currently supported
 @code
 int cvtConvertCol =
 [m_engine getCVColorConvertCode:GL_RGBA thatURL:GL_RGB];
 @endcode
 @param fromGLCol
 The GL PixelFormat to convert from. eg GL_RGBA
 @param toGLCol
 The GL PixelFormat to convert to. eg GL_RGBA
 @return The CVColor conversion code if found, otherwise -1
 */
-(int) getCVColorConvertCode:(int)fromGLCol toGLCol:(int)toGLCol
{
    if(fromGLCol == toGLCol)        { return -1;    }
    
    if(fromGLCol == GL_RGB)
    {
        if(     toGLCol == GL_RGBA)     { return COLOR_RGB2RGBA; }
    }
    else if(fromGLCol == GL_RGBA)
    {
        if(     toGLCol == GL_RGB)      { return COLOR_RGBA2RGB; }
    }
    
    return -1;
}
/**
 Attempts to determine the OpenCV color conversion code
 for the current camera mat.
 
 Note that only GL_RGBA and GL_RGB are currently supported
 @param toGLCol
 The GL PixelFormat to convert to. eg GL_RGBA
 @return The CVColor conversion code if found, otherwise -1
 */
-(int) getCameraCVColorConvertCode:(int)toGLCol
{
    // for now the internal color code for the camera is assumed to be BGR
    
    {
        if(     toGLCol == GL_RGBA)     { return COLOR_BGR2RGBA;    }
        else if(toGLCol == GL_RGB)      { return COLOR_BGR2RGB;     }
    }
    return -1;
}

-(BOOL)     applyMatAsSubImage:(cv::Mat&)mat
                 intoTextureID:(GLuint)intoTextureID
             inputColourFormat:(GLenum)inputColourFormat
{
    if(intoTextureID == 0) { return NO; }
    
    GLuint textureID = intoTextureID;
    
    // Bind to our texture handle
    glBindTexture(GL_TEXTURE_2D, textureID);
    
    glTexSubImage2D(GL_TEXTURE_2D,
                    0,
                    0,
                    0,
                    mat.cols,
                    mat.rows,
                    inputColourFormat,
                    GL_UNSIGNED_BYTE,
                    mat.ptr());
    
    glBindTexture(GL_TEXTURE_2D, 0);
    return YES;
}

-(BOOL) generateNewDestRenderTexture:(GLenum)format
{
    GLuint  newTexID;
    
    int     colorChannels;
    
    if      (format == GL_RGB)  { colorChannels = 3;    }
    else if (format == GL_RGBA) { colorChannels = 4;    }
    else                        { return NO;            }
    
    // TODO: create some sort of algorithm to work out best sized texture
    //  for now we'll just choose the nearest power of two (ceil)
    int     newTexW;
    int     newTexH;
    {
        int camW            = [self getRenderWidth];
        int camH            = [self getRenderHeight];
        int maxCamDimension = camW > camH ? camW : camH;
        
        int nPOT            = 1;
        while (maxCamDimension > nPOT)  { nPOT<<=1; }
        
        if(nPOT == 1)   { return NO; }
        if(nPOT > 4096) { return NO; }
        
        newTexW     = nPOT;
        newTexH     = nPOT;
    }
    
    // initialise our pixels
    unsigned char* pixels = new unsigned char[newTexW*newTexH*colorChannels];
    {
        unsigned char* dst = pixels;
        int stride = newTexH*colorChannels;
        for (int y = 0; y < newTexH; ++y)
        {
            unsigned char* ptr = dst;
            for (int x = 0; x < newTexW; ++x)
            {
                // Write the texture pixel
                for (int c = 0; c < colorChannels; c++) {
                    ptr[c] = 0;
                }
                
                // To next pixel (our pixels are 4 bpp)
                ptr += colorChannels;
            }
            
            // To next image row
            dst += stride;
        }
    }
    
    // generate our texture
    glGenTextures(1, &newTexID);
    glBindTexture(GL_TEXTURE_2D, newTexID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    glTexImage2D(GL_TEXTURE_2D, 0, format, newTexW, newTexH, 0, format, GL_UNSIGNED_BYTE, pixels);
    delete[] pixels;
    
    [self setTextureID:newTexID texW:newTexW texH:newTexH texFormat:format];
    
    return YES;
}

#pragma mark Locks

- (BOOL) lockCurrentCameraFrame
{
    int result = pthread_mutex_lock(&m_mutexCameraFrame);
    return (result==0);
}
- (BOOL) tryLockCurrentCameraFrame
{
    int result = pthread_mutex_trylock(&m_mutexCameraFrame);
    return (result==0);
}
- (void) unlockCurrentCameraFrame
{
    pthread_mutex_unlock(&m_mutexCameraFrame);
}

@end