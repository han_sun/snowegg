//
//  2DMatcher.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 4/03/2015.
//
//

#include "2DMatcher.h"
#include "../Feature/FeaturePoint.h"

using namespace SnowEgg;
using namespace Matcher;

C2DMatcher::C2DMatcher()
{
    m_dKNNRatio = RATIO_KNN;
    m_iDesDataType = TYPE_DESC_DATA;
    m_iMaxIter = NUM_MAX_ITER;
    m_pFlannIndex = NULL;
    
    m_pMatFlannStorage = NULL;
}

C2DMatcher::~C2DMatcher()
{
    if (0 != m_matFlannStorage.size)
    {
        m_matFlannStorage.release();
    }
    
    SAFE_DELETE(m_pFlannIndex);
}

bool C2DMatcher::Train(std::vector<Feature::CFeaturePoint*>* pListFeaturePoint)
{
    if (NULL == pListFeaturePoint)
    {
        return false;
    }
    
    int iListSize = static_cast<int>(pListFeaturePoint->size());
    
    if (iListSize <= 0)
    {
        return false;
    }
    
    int iDim = (*pListFeaturePoint)[0]->GetDesDim();
    
    if(NULL != m_pMatFlannStorage)
    {
        cvReleaseMat(&m_pMatFlannStorage);
    }
    
    m_pMatFlannStorage = cvCreateMat(iListSize, iDim, TYPE_DESC_DATA);
    
    for(int y = 0; y < iListSize; ++y)
    {
        for(int x = 0; x < iDim; ++x)
        {
            cvSetReal2D(m_pMatFlannStorage, y, x, ((*pListFeaturePoint)[y]->GetDescriptors())[x]);
        }
    }
    
    m_matFlannStorage = cv::Mat(m_pMatFlannStorage, false);
    
    SAFE_DELETE(m_pFlannIndex);
    m_pFlannIndex = new cv::flann::Index(m_matFlannStorage, cv::flann::KDTreeIndexParams(2));

    return true;
}

int C2DMatcher::Match(Feature::CFeaturePoint* pTestPoint, double* dDistance)
{
    if (NULL == pTestPoint || NULL == m_pFlannIndex)
    {
        return -1;
    }
    
    int iDim = pTestPoint->GetDesDim();
    
    CvMat* pMatTestDesc = cvCreateMat(1, iDim, TYPE_DESC_DATA);
    cv::Mat matKnnIndex(1, 2, CV_32S);
    cv::Mat matKnnDistance(1, 2, CV_32FC1);
    
    for (int i = 0; i < iDim; ++i)
    {
        cvSetReal2D(pMatTestDesc, 0, i, (pTestPoint->GetDescriptors())[i]);
    }
    
    cv::Mat matTestDesc(pMatTestDesc, false);
    
    m_pFlannIndex->knnSearch(matTestDesc, matKnnIndex, matKnnDistance, 2, cv::flann::SearchParams(m_iMaxIter));
    
    int iIndex0 = matKnnIndex.ptr<int>(0)[0];
    int iIndex1 = matKnnIndex.ptr<int>(0)[1];
    
    double dDis0 = static_cast<double>(matKnnDistance.ptr<float>(0)[0]);
    double dDis1 = static_cast<double>(matKnnDistance.ptr<float>(0)[1]);
    
    int iMatchedIndex = -1;
    double dMinDis = 0.0f;
    double dMaxDis = 0.0f;
    
    if (dDis0 < dDis1)
    {
        dMinDis = dDis0;
        dMaxDis = dDis1;
        iMatchedIndex = iIndex0;
    }
    else
    {
        dMinDis = dDis1;
        dMaxDis = dDis0;
        iMatchedIndex = iIndex1;
    }
    
    if (dMinDis > dMaxDis * m_dKNNRatio)
    {
        iMatchedIndex = -1;
    }
    else
    {
        if (NULL != dDistance)
        {
            *dDistance = dMinDis;
        }
    }
    
    cvReleaseMat(&pMatTestDesc);

    return iMatchedIndex;
}

C2DMatchedPair* C2DMatcher::Match(Feature::CFeaturePoint* pTestPoint)
{
    if (NULL == pTestPoint)
    {
        return NULL;
    }
    
    double dDistance = 0.0f;
    int iMathedIndex = -1;
    
    iMathedIndex = this->Match(pTestPoint, &dDistance);
    
    if (iMathedIndex < 0)
    {
        return NULL;
    }
    
    C2DMatchedPair* pMatchedPair = new C2DMatchedPair();
    pMatchedPair->SetDistance(dDistance);
    // TODO: set it to the pointer point to the repo point
    //pMatchedPair->SetRefFeaturePoint()
    
    return pMatchedPair;
}

double C2DMatcher::GetKNNRatio() const
{
    return m_dKNNRatio;
}

void C2DMatcher::SetKNNRatio(const double dKNNRatio)
{
    m_dKNNRatio = dKNNRatio;
}

void C2DMatcher::SetMaxIter(int iMaxIter)
{
    if (iMaxIter <= 0)
    {
        return;
    }
    
    m_iMaxIter = iMaxIter;
}

int C2DMatcher::GetMaxIter() const
{
    return m_iMaxIter;
}