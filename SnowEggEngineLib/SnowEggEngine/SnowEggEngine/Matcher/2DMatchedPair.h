//
//  2DMatchedPair.h
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#ifndef __2DMATCHEDPAIR_H__
#define __2DMATCHEDPAIR_H__

#include "MatchedPair.h"

namespace SnowEgg
{
    namespace Matcher
    {
        class C2DMatchedPair : virtual public CMatchedPair
        {
        public:
            C2DMatchedPair();
            virtual ~C2DMatchedPair();
            
        public:
            virtual void SetRefFeaturePoint(Feature::CFeaturePoint* pRefFP);
            virtual void SetSceFeaturePoint(Feature::CFeaturePoint* pSceFP);
            virtual Feature::CFeaturePoint* GetRefFeaturePoint();
            virtual Feature::CFeaturePoint* GetSceFeaturePoint();
            
        public:
            virtual void SetDistance(double dDistance);
            virtual double GetDistance() const;
        
        public:
            bool IsInliner();
            void SetIsInliner(bool bIsInliner);
            bool IsTracking();
            void SetIsTracking(bool bIsTracking);
            
        private:
            bool m_bIsInliner;
            bool m_bIsTracking;
        };
    }
}


#endif /* defined(__2DMATCHEDPAIR_H__) */
