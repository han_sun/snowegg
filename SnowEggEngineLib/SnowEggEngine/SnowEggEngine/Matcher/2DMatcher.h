//
//  2DMatcher.h
//  SnowEggEngine
//
//  Created by Han Sun on 4/03/2015.
//
//

#ifndef __2DMATCHER_H__
#define __2DMATCHER_H__

#include "../Base/OpenCVIOSAdapt.h"
#include "../Base/Defines.h"
#include "Matcher.h"
#include "2DMatchedPair.h"

namespace SnowEgg
{
    namespace Matcher
    {
        class C2DMatcher : virtual public CMatcher
        {
        public:
            C2DMatcher();
            virtual ~C2DMatcher();
            
        public:
            virtual bool Train(std::vector<Feature::CFeaturePoint*>* pListFeaturePoint);
            virtual int Match(Feature::CFeaturePoint* pTestPoint, double* dDistance = NULL);
            virtual C2DMatchedPair* Match(Feature::CFeaturePoint* pTestPoint);
            
        public:
            double GetKNNRatio() const;
            void SetKNNRatio(const double dKNNRatio);
            
        public:
            inline void SetMaxIter(int iMaxIter);
            inline int GetMaxIter() const;
            
        protected:
            int m_iDesDataType;
            double m_dKNNRatio;
            int m_iMaxIter;
            CvMat* m_pMatFlannStorage;
            cv::Mat m_matFlannStorage;
            cv::flann::Index* m_pFlannIndex;
        };
    }
}

#endif /* defined(__2DMATCHER_H__) */
