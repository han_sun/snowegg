//
//  2DMatchedPair.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#include "2DMatchedPair.h"

using namespace SnowEgg;
using namespace Matcher;

C2DMatchedPair::C2DMatchedPair()
{
    m_bIsInliner = false;
    m_bIsTracking = false;
}

C2DMatchedPair::~C2DMatchedPair()
{
    
}

void C2DMatchedPair::SetRefFeaturePoint(Feature::CFeaturePoint* pRefFP)
{
    m_pRefFP = pRefFP;
}

void C2DMatchedPair::SetSceFeaturePoint(Feature::CFeaturePoint* pSceFP)
{
    if (NULL != m_pSceFP)
    {
        SAFE_DELETE(m_pSceFP);
    }
    
    m_pSceFP = pSceFP;
}

Feature::CFeaturePoint* C2DMatchedPair::GetRefFeaturePoint()
{
    return m_pRefFP;
}

Feature::CFeaturePoint* C2DMatchedPair::GetSceFeaturePoint()
{
    return m_pSceFP;
}

void C2DMatchedPair::SetDistance(double dDistance)
{
    m_dDistance = dDistance;
}

double C2DMatchedPair::GetDistance() const
{
    return m_dDistance;
}

bool C2DMatchedPair::IsInliner()
{
    return m_bIsInliner;
}

void C2DMatchedPair::SetIsInliner(bool bIsInliner)
{
    m_bIsInliner = bIsInliner;
}

bool C2DMatchedPair::IsTracking()
{
    return m_bIsTracking;
}

void C2DMatchedPair::SetIsTracking(bool bIsTracking)
{
    m_bIsTracking = bIsTracking;
}


