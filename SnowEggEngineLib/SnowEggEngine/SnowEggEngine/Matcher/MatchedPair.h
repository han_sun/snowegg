//
//  MatchedPair.h
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#ifndef __MATCHEDPAIR_H__
#define __MATCHEDPAIR_H__

#include "../Feature/FeaturePoint.h"
#include "../Base/Defines.h"

namespace SnowEgg
{
    namespace Matcher
    {
        class CMatchedPair
        {
        public:
            CMatchedPair();
            virtual ~CMatchedPair();
            
        public:
            virtual void SetRefFeaturePoint(Feature::CFeaturePoint* pRefFP) = 0;
            virtual void SetSceFeaturePoint(Feature::CFeaturePoint* pSceFP) = 0;
            virtual Feature::CFeaturePoint* GetRefFeaturePoint() = 0;
            virtual Feature::CFeaturePoint* GetSceFeaturePoint() = 0;
            
        public:
            virtual void SetDistance(double dDistance) = 0;
            virtual double GetDistance() const = 0;
            
        public:
            int Attenuate(int iAttenuation = DEF_ATTENUATION);
            int GetQuality() const;
            
        protected:
            Feature::CFeaturePoint* m_pRefFP;
            Feature::CFeaturePoint* m_pSceFP;
            double m_dDistance;
            int m_iQuality;
        };
    }
}

#endif /* defined(__MATCHEDPAIR_H__) */
