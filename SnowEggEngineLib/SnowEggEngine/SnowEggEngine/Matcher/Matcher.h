//
//  Matcher.h
//  SnowEggEngine
//
//  Created by Han Sun on 4/03/2015.
//
//

#ifndef __MATCHER_H__
#define __MATCHER_H__

#include <vector>
#include "MatchedPair.h"
#include "../Feature/FeaturePoint.h"

namespace SnowEgg
{
    namespace Matcher
    {
        class CMatcher
        {
        public:
            CMatcher();
            virtual ~CMatcher();
            
        public:
            // Train with input feature points
            virtual bool Train(std::vector<Feature::CFeaturePoint*>* pListFeaturePoint) = 0;
            // return -1 if no matching
            // return index of matched point in trained data
            // pTestPoint is the testing data.
            // can use dDistance to get the distance between testing data and matched data
            virtual int Match(Feature::CFeaturePoint* pTestPoint, double* dDistance = NULL) = 0;
            // TODO: add a overload Match method, return a matched pair
            virtual CMatchedPair* Match(Feature::CFeaturePoint* pTestPoint) = 0;
        };
    }
}

#endif /* defined(__MATCHER_H__) */
