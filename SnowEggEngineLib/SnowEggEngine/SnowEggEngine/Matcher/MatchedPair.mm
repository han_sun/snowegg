//
//  MatchedPair.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 9/03/2015.
//
//

#include "MatchedPair.h"

using namespace SnowEgg;
using namespace Matcher;

CMatchedPair::CMatchedPair()
{
    m_pSceFP = NULL;
    m_pRefFP = NULL;
    
    m_dDistance = 0.0f;
    m_iQuality = INIT_QUALITY + rand()%INIT_QUALITY;
}

CMatchedPair::~CMatchedPair()
{
    SAFE_DELETE(m_pSceFP);
    m_pRefFP = NULL;
    m_dDistance = 0.0f;
}

int CMatchedPair::Attenuate(int iAttenuation)
{
    m_iQuality -= iAttenuation;
    return m_iQuality;
}

int CMatchedPair::GetQuality() const
{
    return m_iQuality;
}