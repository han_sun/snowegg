//
//  SURFDescriptor.h
//  SnowEggEngine
//
//  Created by Han Sun on 3/03/2015.
//
//

#ifndef __SURFEXTRACTOR_H__
#define __SURFEXTRACTOR_H__

#include "../Base/OpenCVIOSAdapt.h"

namespace SnowEgg
{
    namespace Feature
    {
        class CSURFExtractor
        {
        public:
            static void ExtractSURF(const CvArr* _img, const CvArr* _mask,
                             CvSeq** _keypoints, CvSeq** _descriptors,
                             CvMemStorage* storage, CvSURFParams params);
        };
    }
}

#endif /* defined(__SURFEXTRACTOR_H__) */
