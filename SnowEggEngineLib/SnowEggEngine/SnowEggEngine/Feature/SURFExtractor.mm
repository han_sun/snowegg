//
//  SURFDescriptor.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 3/03/2015.
//
//  Divid the patch into 3 * 3 sub tiles, each tile is 5s width.
//  Do haar wavelets response calculation for each tile,
//  Use v(Sig(dx), Sig(d|x|), Sig(dy), Sig(d|y|)) as descriptor
//  Descriptor's dim = 4 * 9 = 36
//
//  TODO: More optimization can be applied, follow the paper:
//  "Simplifying SURF Feature Descriptor to Achieve Real-Time Performance", Marek Kraft, Adam Schmidt, 2011

#include "SURFExtractor.h"

using namespace SnowEgg;
using namespace Feature;

// haar wavelets template
const int dx1[] = {3, 3, 2, 1, 0, -1, -2, -3};
const int dy1[] = {0, 1, 2, 3, 3, 3, 2, 1};
const int dx2[] = {-3, -3, -2, -1, 0, 1, 2, 3};
const int dy2[] = {0, -1, -2, -3, -3, -3, -2, -1};

void CSURFExtractor::ExtractSURF(const CvArr* _img, const CvArr* _mask,
                      CvSeq** _keypoints, CvSeq** _descriptors,
                      CvMemStorage* storage, CvSURFParams params)
{
    if (NULL != _descriptors)
    {
        *_descriptors = 0;
    }
    
    // using given key points
    CvSeq *keypoints = *_keypoints;
    int N = keypoints->total;
    
    IplImage* image = ((IplImage*)_img);
    
    // reduce patch size.
    // original is 20.
    const int PATCH_SZ = 15;
    
    // make descriptor size smaller than openCV version.
    // original is 128 (extended) or 64(normal).
    int descriptor_size = 36;
    const int descriptor_data_type = CV_32F;
    CvSeq* descriptors = 0;
    if (NULL != _descriptors)
    {
        descriptors = cvCreateSeq(0, sizeof(CvSeq),
                                  descriptor_size * CV_ELEM_SIZE(descriptor_data_type),
                                  storage);
        cvSeqPushMulti(descriptors, 0, N);
    }
    
    for (int k = 0; k < N; ++k)
    {
        int i, j;
        CvSURFPoint* kp = (CvSURFPoint*)cvGetSeqElem(keypoints, k);
        int x = cvRound(kp->pt.x);
        int y = cvRound(kp->pt.y);
        
        // calculate rotation
        int dx = 0;
        int dy = 0;
        for (int i = 0; i < 8; ++i)
        {
            int difference = (int)((unsigned char)image->imageData[(y + dy1[i]) * image->widthStep + (x + dx1[i])]
                                   - (unsigned char)image->imageData[(y + dy2[i]) * image->widthStep + (x + dx2[i])]);
            dx += dx1[i] * difference;
            dy += dy1[i] * difference;
        }
        
        float descriptor_dir = cvFastArctan((float)dy, (float)dx);
        kp->dir = descriptor_dir;
        descriptor_dir *= (float)(CV_PI/180);
        
        // extract descriptors
        float sin_dir = sin(-descriptor_dir);
        float cos_dir = cos(-descriptor_dir) ;
        
        int PATCH[PATCH_SZ+1][PATCH_SZ+1];
        cvMat(PATCH_SZ+1, PATCH_SZ+1, CV_8U, PATCH);
        
        // Nearest neighbour version (faster)
        float win_offset = -(float)(PATCH_SZ-1)/2;
        float start_x = kp->pt.x + win_offset*cos_dir + win_offset*sin_dir;
        float start_y = kp->pt.y - win_offset*sin_dir + win_offset*cos_dir;
        for (i = 0; i < PATCH_SZ + 1; ++i, start_x += sin_dir, start_y += cos_dir)
        {
            float pixel_x = start_x;
            float pixel_y = start_y;
            for (j = 0; j < PATCH_SZ + 1; ++j, pixel_x += cos_dir, pixel_y -= sin_dir)
            {
                x = cvRound(pixel_x);
                y = cvRound(pixel_y);
                x = MAX(x, 0);
                y = MAX(y, 0);
                x = MIN(x, image->width-1);
                y = MIN(y, image->height-1);
                PATCH[i][j] = (int)(unsigned char)image->imageData[y*image->widthStep + x];
            }
        }
        
        // Calculate gradients in x and y with wavelets of size 2s
        float DX[PATCH_SZ][PATCH_SZ];
        float DY[PATCH_SZ][PATCH_SZ];
        float dw = 1.0f;
        for (i = 0; i < PATCH_SZ; ++i)
        {
            for (j = 0; j < PATCH_SZ; ++j)
            {
                DX[i][j] = (float)(PATCH[i][j+1] - PATCH[i][j] + PATCH[i+1][j+1] - PATCH[i+1][j]) * dw;
                DY[i][j] = (float)(PATCH[i+1][j] - PATCH[i][j] + PATCH[i+1][j+1] - PATCH[i][j+1]) * dw;
            }
        }
        
        // Construct the descriptor
        float* vec = (float*)cvGetSeqElem(descriptors, k);
        for (i = 0; i < descriptor_size; ++i)
        {
            vec[i] = 0;
        }

        // always 36-bin descriptor
        for (i = 0; i < 3; ++i)
        {
            for (j = 0; j < 3; ++j)
            {
                for (y = i*5; y < i*5+5; ++y)
                {
                    for (x = j*5; x < j*5+5; ++x)
                    {
                        vec[0] += DX[y][x];
                        vec[1] += DY[y][x];
                        vec[2] += fabs(DX[y][x]);
                        vec[3] += fabs(DY[y][x]);
                    }
                }
                vec += 4;
            }
        }
    }
    
    if (NULL != _descriptors)
    {
        *_descriptors = descriptors;
    }
    
    if (NULL != _keypoints)
    {
        *_keypoints = keypoints;
    }
}