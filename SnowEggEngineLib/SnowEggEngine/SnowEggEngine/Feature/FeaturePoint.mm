//
//  FeaturePoint.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 4/03/2015.
//
//

#include <math.h>
#include "FeaturePoint.h"

using namespace SnowEgg;
using namespace Feature;

CFeaturePoint::CFeaturePoint()
{
    m_pos.SetZero();
    m_pos.z = 1.0f;
    
    m_color.val[0] = 0.0f;
    m_color.val[1] = 0.0f;
    m_color.val[2] = 0.0f;
    m_color.val[3] = 0.0f;
    
    m_iRepoIndex = -1;
    m_iTrackableID = -1;
    
    m_iSize = 0;
    m_dDir = 0.0f;
    
    m_iCornerScore = 0;
    m_iDesDim = 36;
    m_vecDescriptors.resize(m_iDesDim);
}

CFeaturePoint::~CFeaturePoint()
{
    
}