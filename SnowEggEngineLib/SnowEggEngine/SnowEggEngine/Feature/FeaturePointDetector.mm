//
//  FeaturePointDetector.cpp
//  SnowEggEngine
//
//  Created by Han Sun on 12/03/2015.
//
//

#include "../Base/OpenCVIOSAdapt.h"
#include "FeaturePointDetector.h"
#include "FASTCornerDetector.h"
#include "SURFExtractor.h"

using namespace SnowEgg;
using namespace Feature;

CFeaturePointDetector::CFeaturePointDetector()
{
    m_dThreshold = THRES_DETECT;
}

CFeaturePointDetector::~CFeaturePointDetector()
{

}

bool CFeaturePointDetector::Detect(IplImage* grayImage, std::vector<CFeaturePoint*>* pVecListFP, int iMax, int iThrDet, std::vector<Matcher::C2DMatchedPair*>* pVecExists)
{
    if(NULL == grayImage)
    {
        return false;
    }
    
    if(1 != grayImage->nChannels)
    {
        return false;
    }
    
    pVecListFP->clear();
    
    // Extract FAST corners;
    CvSeq* descriptors;
    CvMemStorage* storage = cvCreateMemStorage(0);
    CvSeq* keypointsSeq = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvSURFPoint), storage);
    
    // Set Exists
    xy* pExist = NULL;
    int iExistCount = 0;
    if (NULL != pVecExists)
    {
        iExistCount = static_cast<int>(pVecExists->size());
        pExist = new xy[iExistCount];
        
        for (int i = 0; i < iExistCount; ++i)
        {
            pExist[i].x = (*pVecExists)[i]->GetSceFeaturePoint()->GetPosition().x;
            pExist[i].y = (*pVecExists)[i]->GetSceFeaturePoint()->GetPosition().y;
        }
    }
    
    int cornerCount = 0;
    xy* cornerPoints = NULL;
    cornerPoints = CFastCornerDetector::fast9_detect_nonmax((const byte*)grayImage->imageData,
                                                            grayImage->width,
                                                            grayImage->height,
                                                            grayImage->widthStep,
                                                            cvRound(iThrDet),
                                                            &cornerCount, pExist, iExistCount);
    
    if (iMax != 0)
    {
        float fStep = static_cast<float>(cornerCount) / static_cast<float>(iMax);

        for (float i = 0.0f; static_cast<int>(i) < cornerCount; i += fStep)
        {
            CvSURFPoint point = cvSURFPoint(cvPoint2D32f(cornerPoints[static_cast<int>(i)].x, cornerPoints[static_cast<int>(i)].y), 0, 15, 0, 0);
            cvSeqPush(keypointsSeq, &point);
        }
    }
    else
    {
        for (int i = 0; i < cornerCount; ++i)
        {
            CvSURFPoint point = cvSURFPoint(cvPoint2D32f(cornerPoints[i].x, cornerPoints[i].y), 0, 15, 0, 0);
            cvSeqPush(keypointsSeq, &point);
        }
    }
    
    SAFE_DELETE_ARR(pExist);
    SAFE_DELETE_ARR(cornerPoints);

    // Generate Descriptor
    CvSURFParams params = cvSURFParams(m_dThreshold, 0);
    
    CSURFExtractor::ExtractSURF(grayImage, NULL, &keypointsSeq, &descriptors, storage, params);
    
    CvSeqReader reader;
    cvStartReadSeq(descriptors, &reader, 0);
    
    for(int i = 0; i< keypointsSeq->total; ++i)
    {
        CFeaturePoint* pNewPoint = new CFeaturePoint();
        CvSURFPoint* surfPT = (CvSURFPoint*)cvGetSeqElem(keypointsSeq, i);
        
        pNewPoint->SetPosition(CVector3f(surfPT->pt.x, surfPT->pt.y, 1.0));
        pNewPoint->SetSize(surfPT->size);
        pNewPoint->SetDir(surfPT->dir);
        
        const float* vec = (const float*)reader.ptr;
        CV_NEXT_SEQ_ELEM(reader.seq->elem_size, reader);

        int iDim = pNewPoint->GetDesDim();
        
        for(int j=0; j < iDim; ++j)
        {
            pNewPoint->GetDescriptors()[j] = vec[j];
        }
        
        pVecListFP->push_back(pNewPoint);
    }
    
    cvReleaseMemStorage(&storage);
    
    return true;
}