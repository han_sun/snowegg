//
//  FASTCornerDetector.h
//  SnowEggEngine
//
//  Created by Han Sun on 2/03/2015.
//
//

#ifndef __FASTCORNERDETECTOR_H__
#define __FASTCORNERDETECTOR_H__

#define STEP_SIZE_X 1
#define STEP_SIZE_Y 2

namespace SnowEgg
{
    namespace Feature
    {
        typedef struct { int x, y; } xy;
        typedef unsigned char byte;
        
        class CFastCornerDetector
        {
        public:
            static xy* fast9_detect_nonmax(const byte* im, int xsize, int ysize, int stride, int b, int* ret_num_corners, xy* exists = NULL, int iExistCount = 0);
            
        private:
            static xy* fast9_detect(const byte* im, int xsize, int ysize, int stride, int b, int* ret_num_corners, int stepx=STEP_SIZE_X, int stepy=STEP_SIZE_Y);
            static int* fast9_score(const byte* i, int stride, xy* corners, int num_corners, int b);
            static xy* nonmax_suppression(const xy* corners, const int* scores, int num_corners, int* ret_num_nonmax);
            static int fast9_corner_score(const byte* p, const int pixel[], int bstart);
        };
    }
}

#endif /* defined(__FASTCORNERDETECTOR_H__) */
