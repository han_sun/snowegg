//
//  FeaturePoint.h
//  SnowEggEngine
//
//  Created by Han Sun on 4/03/2015.
//
//

#ifndef __FEATUREPOINT_H__
#define __FEATUREPOINT_H__

#include <vector>
#include "../Base/OpenCVIOSAdapt.h"
#include "../Base/Vector3f.h"

namespace SnowEgg
{
    namespace Feature
    {
        class CFeaturePoint
        {
        public:
            CFeaturePoint();
            virtual ~CFeaturePoint();
            
        public:
            virtual double CalDistance(CFeaturePoint& other);
            
        public:
            virtual void operator = (CFeaturePoint& right);
            
        public:
            void SetPosition(const CVector3f& pos);
            CVector3f GetPosition() const;
            void SetColor(const CvScalar& color);
            CvScalar GetColor() const;
            void SetRepoIndex(const int iRepoIndex);
            int GetRepoIndex() const;
            void SetTrackableID(const int iTrackableID);
            int GetTrackableID() const;
            void SetSize(const int iSize);
            int GetSize() const;
            void SetDir(const double dDir);
            double GetDir() const;
            void SetDesDim(const int iDesDim);
            int GetDesDim() const;
            void SetScore(int iScore);
            int GetScore() const;
            std::vector<double>& GetDescriptors();
            
        protected:
            CVector3f m_pos; // as a 2D point, z = 1.0f
            CvScalar m_color;
            int m_iRepoIndex; // index in it's repository
            int m_iTrackableID; // the id of the trackable it belonged
            int m_iSize; // feature size
            double m_dDir; // feature main direction
            int m_iDesDim; // descriptor's dimension
            int m_iCornerScore;
            std::vector<double> m_vecDescriptors;
        };
        
        inline void CFeaturePoint::operator = (CFeaturePoint& right)
        {
            m_pos = right.GetPosition();
            m_color = right.GetColor();
            m_iRepoIndex = right.GetRepoIndex();
            m_iTrackableID = right.GetTrackableID();
            m_iSize = right.GetSize();
            m_dDir = right.GetDir();
            m_iCornerScore = right.GetScore();
            
            m_iDesDim = right.GetDesDim();
            m_vecDescriptors.resize(m_iDesDim);
            for(int i = 0; i < m_iDesDim; ++i)
            {
                m_vecDescriptors[i] = right.m_vecDescriptors[i];
            }
        }
        
        inline double CFeaturePoint::CalDistance(CFeaturePoint& other)
        {
            if(m_iDesDim != other.m_iDesDim)
            {
                return -1.0f;
            }
            
            double dSum = 0.0f;
            for(int i = 0; i < m_iDesDim; ++i)
            {
                dSum += abs(m_vecDescriptors[i] - other.m_vecDescriptors[i]);
            }
            
            return dSum;
        }
        
        inline void CFeaturePoint::SetPosition(const CVector3f& pos)
        {
            m_pos = pos;
        }
        
        inline CVector3f CFeaturePoint::GetPosition() const
        {
            return m_pos;
        }
        
        inline void CFeaturePoint::SetColor(const CvScalar& color)
        {
            m_color = color;
        }
        
        inline CvScalar CFeaturePoint::GetColor() const
        {
            return m_color;
        }
        
        inline void CFeaturePoint::SetRepoIndex(const int iRepoIndex)
        {
            m_iRepoIndex = iRepoIndex;
        }
        
        inline int CFeaturePoint::GetRepoIndex() const
        {
            return m_iRepoIndex;
        }
        
        inline void CFeaturePoint::SetTrackableID(const int iTrackableID)
        {
            m_iTrackableID = iTrackableID;
        }
        
        inline int CFeaturePoint::GetTrackableID() const
        {
            return m_iTrackableID;
        }
        
        inline void CFeaturePoint::SetSize(const int iSize)
        {
            m_iSize = iSize;
        }
        
        inline int CFeaturePoint::GetSize() const
        {
            return m_iSize;
        }
        
        inline void CFeaturePoint::SetDir(const double dDir)
        {
            m_dDir = dDir;
        }
        
        inline double CFeaturePoint::GetDir() const
        {
            return m_dDir;
        }
        
        inline void CFeaturePoint::SetDesDim(const int iDesDim)
        {
            m_iDesDim = iDesDim;
        }
        
        inline int CFeaturePoint::GetDesDim() const
        {
            return  m_iDesDim;
        }
        
        inline void CFeaturePoint::SetScore(int iScore)
        {
            m_iCornerScore = iScore;
        }
        
        inline int CFeaturePoint::GetScore() const
        {
            return m_iCornerScore;
        }
        
        inline std::vector<double>& CFeaturePoint::GetDescriptors()
        {
            return m_vecDescriptors;
        }
    }
}

#endif /* defined(__FEATUREPOINT_H__) */
