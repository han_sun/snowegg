//
//  FeaturePointDetector.h
//  SnowEggEngine
//
//  Created by Han Sun on 12/03/2015.
//
//

#ifndef __FEATUREPOINTDETECTOR_H__
#define __FEATUREPOINTDETECTOR_H__

#include <stdio.h>
#include <vector>
#include "FeaturePoint.h"
#include "../Matcher/2DMatchedPair.h"
#include "../Base/Defines.h"

namespace SnowEgg
{
    namespace Feature
    {
        class CFeaturePointDetector
        {
        public:
            CFeaturePointDetector();
            ~CFeaturePointDetector();
            
        public:
            bool Detect(IplImage* grayImage, std::vector<CFeaturePoint*>* pVecListFP, int iMax = 0, int iThrDet = THRES_DETECT, std::vector<Matcher::C2DMatchedPair*>* pVecExists = NULL);
            
        protected:
            double m_dThreshold;
        };
    }
}

#endif /* defined(__FEATUREPOINTDETECTOR_H__) */
