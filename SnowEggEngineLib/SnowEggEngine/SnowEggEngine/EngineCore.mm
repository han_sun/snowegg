//
//  TestRenderer.cpp
//  SnowEgg
//
//  Created by Han Sun on 24/02/2015.
//
//

#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import "Base/OpenCVIOSAdapt.h"
#import "EngineCore.h"
#include "Showcase/ShowcaseFactory.h"

using namespace SnowEgg;

CEngineCore* CEngineCore::GetInstance()
{
    static CEngineCore instance;
    return &instance;
}

CEngineCore::CEngineCore()
{
    m_bStartTracking = false;
    m_pCamDevice = new CCameraDevice();
}

bool CEngineCore::InitEngine()
{
    // TODO: Set Callback pointer to camera
    bool ret;
    
    ret = m_pCamDevice->InitCam();
    
    if(!ret)
    {
        // TODO: cleanup?
    }
    
    return ret;
}

void CEngineCore::StartEngine()
{
    m_pCamDevice->StartCam();
    m_pCamDevice->SetCamFeedUpdatedCallback(&CEngineCore::CallBackImgProc, (void*)this);
}

void CEngineCore::StopEngine()
{

}

CCameraDevice* CEngineCore::GetCameraDevice() const
{
    return m_pCamDevice;
}

void CEngineCore::ProcessImage(cv::Mat& image)
{
    if (m_bStartTracking)
    {
        int iSize = static_cast<int>(m_vecShowcases.size());
        for (int i = 0; i < iSize; ++i)
        {
            m_vecShowcases[i]->UpdateCamPose(image);
        }
    }
}

void CEngineCore::CallBackImgProc(cv::Mat& image, void* context)
{
    CEngineCore* engine = (CEngineCore*)context;
    engine->ProcessImage(image);
}

char* CEngineCore::CreateShowcase(const char* szShowcaseID, int iType, int iMaxTracking)
{
    char* pNewShowcaseID = NULL;
    
    Showcase::CShowcase* pNewShowcase = Showcase::CShowcaseFactory::Create(iType);
    
    if (NULL != pNewShowcase)
    {
        pNewShowcase->SetMaxTracking(iMaxTracking);
        pNewShowcase->SetID(szShowcaseID);
        m_vecShowcases.push_back(pNewShowcase);
        pNewShowcaseID = pNewShowcase->GetID();
    }
    
    return pNewShowcaseID;
}

bool CEngineCore::ReleaseShowcase(const char* szShowcaseID)
{
    if (NULL == szShowcaseID)
    {
        return false;
    }
    
    int iListSize = static_cast<int>(m_vecShowcases.size());
    
    for (int i = 0; i < iListSize; ++i)
    {
        if (0 == strcmp(m_vecShowcases[i]->GetID(), szShowcaseID))
        {
            SAFE_DELETE(m_vecShowcases[i]);
            m_vecShowcases.erase(m_vecShowcases.begin() + i);
            
            return true;
        }
    }
    
    return false;
}

bool CEngineCore::ReleaseAllShowcase()
{
    int iListSize = static_cast<int>(m_vecShowcases.size());
    for (int i = 0; i < iListSize; ++i)
    {
        SAFE_DELETE(m_vecShowcases[i]);
    }
    
    m_vecShowcases.clear();
    
    return true;
}

Showcase::CShowcase* CEngineCore::GetShowcase(const char* szShowcaseID)
{
    if (NULL == szShowcaseID)
    {
        return NULL;
    }
    
    int iSize = static_cast<int>(m_vecShowcases.size());
    
    for (int i = 0; i < iSize; ++i)
    {
        if (0 == strcmp(m_vecShowcases[i]->GetID(), szShowcaseID))
        {
            return m_vecShowcases[i];
        }
    }
    
    return NULL;
}

Showcase::CShowcase* CEngineCore::GetShowcaseWithIndex(int iIndex)
{
    int iShowcaseCount = static_cast<int>(m_vecShowcases.size());
    
    if (iIndex < 0 || iIndex >= iShowcaseCount)
    {
        return NULL;
    }
    else
    {
        return m_vecShowcases[iIndex];
    }
}

int CEngineCore::GetShowcaseCount()
{
    return static_cast<int>(m_vecShowcases.size());
}

void CEngineCore::StartTracking()
{
    m_bStartTracking = true;
}

void CEngineCore::StopTracking()
{
    m_bStartTracking = false;
}

void CEngineCore::CallBackDidAccelerate(float x, float y, float z)
{
    if (m_bStartTracking)
    {
        // Do something
    }
}



